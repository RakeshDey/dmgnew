package dmg.com.dmg.UI;

/**
 * Created by Advosoft2 on 4/13/2018.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import dmg.com.dmg.R;

public class DrawPlot extends View {
    private Paint f3030a = new Paint();
    private float[] f3031b = new float[0];
    private float[] f3032c = new float[0];
    private boolean f3033d = true;

    public DrawPlot(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void m4836a(float[] fArr, float[] fArr2) {
        this.f3031b = fArr;
        this.f3032c = fArr2;
        invalidate();
    }

    public void m4837b(float[] fArr, float[] fArr2) {
        this.f3031b = fArr;
        this.f3032c = fArr2;
        this.f3033d = false;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        this.f3030a.setColor(Color.parseColor("#aed581"));
        this.f3030a.setStyle(Style.FILL);
        Path path = new Path();
        int right = getRight() - getLeft();
        if (this.f3031b.length > 2) {
            path.moveTo(this.f3031b[0], Math.abs(this.f3032c[0]));
            for (right = 1; right < this.f3031b.length; right++) {
                path.lineTo(this.f3031b[right], Math.abs(this.f3032c[right]));
            }
            path.lineTo(this.f3031b[0], Math.abs(this.f3032c[0]));
            canvas.drawPath(path, this.f3030a);
        }
        if (this.f3033d) {
            right = getRight() - getLeft();
            this.f3030a.setStrokeWidth(1.0f);
            this.f3030a.setColor(getResources().getColor(R.color.colorPrimary));
            path = new Path();
            path.moveTo((float) (right - 20), 20.0f);
            path.lineTo((float) (right - 25), 25.0f);
            path.lineTo((float) (right - 15), 25.0f);
            path.close();
            canvas.drawPath(path, this.f3030a);
            path.reset();
            path.moveTo((float) (right - 21), 25.0f);
            path.lineTo((float) (right - 21), 100.0f);
            path.lineTo((float) (right - 18), 100.0f);
            path.lineTo((float) (right - 18), 25.0f);
            canvas.drawPath(path, this.f3030a);
            this.f3030a.setTextSize(15.0f);
            canvas.drawText("N", (float) (right - 25), 10.0f, this.f3030a);
        }
        super.onDraw(canvas);
    }
}