package dmg.com.dmg.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Model.MeasureDataModel;
import dmg.com.dmg.R;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class MapsActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback,
        LocationListener, GoogleMap.OnMarkerDragListener {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    @Bind(R.id.save)
    TextView save;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.img_drawing)
    ImageView imgDrawing;
    Polygon polyline;
    SupportMapFragment mapFragment;
    MeasureDataModel model;
    ArrayList<MeasureDataModel> modelList = new ArrayList<>();
    ArrayList<Polyline> polylines = new ArrayList<>();
    ArrayList<Marker> markers = new ArrayList<>();
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    @Bind(R.id.txt_area)
    TextView txtArea;
    @Bind(R.id.txt_bearing)
    TextView txtBearing;
    @Bind(R.id.img_back)
    ImageView imgBack;
    @Bind(R.id.txt_perimeter)
    TextView txtPerimeter;
    @Bind(R.id.txt_distance)
    TextView txtDistance;
    @Bind(R.id.img_delete)
    ImageView imgDelete;
    @Bind(R.id.ll_measure)
    LinearLayout llMeasure;
    private boolean isMap = false;
    private GoogleMap mMap;
    private static String data_null = null;
    private static int data_1 = 1;

    public String result_unit = "";

    private static Bitmap removeMargins(Bitmap bmp, int color) {
        // TODO Auto-generated method stub


        long dtMili = System.currentTimeMillis();
        int MTop = 0, MBot = 0, MLeft = 0, MRight = 0;
        boolean found1 = false, found2 = false;

        int[] bmpIn = new int[bmp.getWidth() * bmp.getHeight()];
        int[][] bmpInt = new int[bmp.getWidth()][bmp.getHeight()];

        bmp.getPixels(bmpIn, 0, bmp.getWidth(), 0, 0, bmp.getWidth(),
                bmp.getHeight());

        for (int ii = 0, contX = 0, contY = 0; ii < bmpIn.length; ii++) {
            bmpInt[contX][contY] = bmpIn[ii];
            contX++;
            if (contX >= bmp.getWidth()) {
                contX = 0;
                contY++;
                if (contY >= bmp.getHeight()) {
                    break;
                }
            }
        }

        for (int hP = 0; hP < bmpInt[0].length && !found2; hP++) {
            // looking for MTop
            for (int wP = 0; wP < bmpInt.length && !found2; wP++) {
                if (bmpInt[wP][hP] != color) {
                    Log.e("MTop 2", "Pixel found @" + hP);
                    MTop = hP;
                    found2 = true;
                    break;
                }
            }
        }
        found2 = false;

        for (int hP = bmpInt[0].length - 1; hP >= 0 && !found2; hP--) {
            // looking for MBot
            for (int wP = 0; wP < bmpInt.length && !found2; wP++) {
                if (bmpInt[wP][hP] != color) {
                    Log.e("MBot 2", "Pixel found @" + hP);
                    MBot = bmp.getHeight() - hP;
                    found2 = true;
                    break;
                }
            }
        }
        found2 = false;

        for (int wP = 0; wP < bmpInt.length && !found2; wP++) {
            // looking for MLeft
            for (int hP = 0; hP < bmpInt[0].length && !found2; hP++) {
                if (bmpInt[wP][hP] != color) {
                    Log.e("MLeft 2", "Pixel found @" + wP);
                    MLeft = wP;
                    found2 = true;
                    break;
                }
            }
        }
        found2 = false;

        for (int wP = bmpInt.length - 1; wP >= 0 && !found2; wP--) {
            // looking for MRight
            for (int hP = 0; hP < bmpInt[0].length && !found2; hP++) {
                if (bmpInt[wP][hP] != color) {
                    Log.e("MRight 2", "Pixel found @" + wP);
                    MRight = bmp.getWidth() - wP;
                    found2 = true;
                    break;
                }
            }

        }
        found2 = false;

        int sizeY = bmp.getHeight() - MBot - MTop, sizeX = bmp.getWidth()
                - MRight - MLeft;

        Bitmap bmp2 = Bitmap.createBitmap(bmp, MLeft, MTop, sizeX, sizeY);
        dtMili = (System.currentTimeMillis() - dtMili);
        Log.e("Margin   2",
                "Time needed " + dtMili + "mSec\nh:" + bmp.getWidth() + "w:"
                        + bmp.getHeight() + "\narray x:" + bmpInt.length + "y:"
                        + bmpInt[0].length);
        return bmp2;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(MapsActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(MapsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {

                    Marker marker = (mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                            .position(latLng)
                            .draggable(true)
                            .title("Lat:" + MyApplication.doubleToDmsNew(latLng.latitude)
                                    // BigDecimal.valueOf(latLng.latitude).setScale(6, RoundingMode.HALF_UP)
                                    + " Long:" + MyApplication.doubleToDmsNew(latLng.longitude)
                            )));

                    markers.add(marker);
                    updatePolygon();
                }
            });
            showModelOnMap(model);
            new CreateBitmap(Bitmap.createBitmap(1500, 1500, Bitmap.Config.ARGB_8888)
                    , getPointList()).execute();
        }
    }

    private void updatePolygon() {
        if (markers.size() > 1) {
            llMeasure.setVisibility(View.VISIBLE);
            if (polyline != null)
                polyline.remove();
            PolygonOptions lineOptions = new PolygonOptions();
            for (int i = 0; i < markers.size(); i++)
                lineOptions.add(markers.get(i).getPosition());

            if (markers.size() > 2) {
                lineOptions.add(markers.get(0).getPosition());
            }
            lineOptions.strokeWidth(5);
            lineOptions.strokeColor(Color.parseColor("#FF5E00"));
            lineOptions.fillColor(Color.parseColor("#7FFF5E00"));
            lineOptions.geodesic(true);
            polyline = mMap.addPolygon(lineOptions);
            com.javadocmd.simplelatlng.LatLng first = new com.javadocmd.simplelatlng.LatLng(
                    markers.get(markers.size() - 2).getPosition().latitude,
                    markers.get(markers.size() - 2).getPosition().longitude);
            com.javadocmd.simplelatlng.LatLng second = new com.javadocmd.simplelatlng.LatLng(
                    markers.get(markers.size() - 1).getPosition().latitude,
                    markers.get(markers.size() - 1).getPosition().longitude);
            calculateBearingDistance(first, second);
        } else {
            llMeasure.setVisibility(View.GONE);
            if (polyline != null)
                polyline.remove();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLastLocation != location) {
            mLastLocation = location;

            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            //Place current location marker
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            //move map camera
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(MapsActivity.this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(MapsActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(MapsActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MapsActivity.this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        model = MyApplication.model;
        setSupportActionBar(toolbar);
        setTitle("Measure By Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.map) {
            if (!isMap) {
                imgDrawing.setVisibility(View.GONE);
                isMap = true;
                if (polyline != null && polyline.getPoints().size() > 0) {
                    llMeasure.setVisibility(View.VISIBLE);
                }
            } else {
                imgDrawing.setVisibility(View.VISIBLE);
                isMap = false;
                llMeasure.setVisibility(View.GONE);
            }
        } else if (id == R.id.open) {
            MyApplication.showOpenDialog(MapsActivity.this, new MyApplication.OpenFile() {
                @Override
                public void onOpenFile(MeasureDataModel model) {
                    showModelOnMap(model);
                }
            });
        } else if (id == android.R.id.home) {
            MyApplication.activityFinish(MapsActivity.this);
        }

        invalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem mapMenu = menu.findItem(R.id.map);
        MenuItem openMenu = menu.findItem(R.id.open);
        // set your desired icon here based on a flag if you like
        if (isMap) {
            mapMenu.setIcon(getResources().getDrawable(R.drawable.ic_show_chart_white_24dp));
            openMenu.setVisible(true);
        } else {
            mapMenu.setIcon(getResources().getDrawable(R.drawable.ic_map_white_24dp));
            openMenu.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.setOnMarkerDragListener(MapsActivity.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MapsActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    private void showModelOnMap(MeasureDataModel model) {
        if (polylines.size() > 0) {
            for (int i = 0; i < polylines.size(); i++) {
                polylines.get(i).remove();
            }
        }

        modelList.add(model);


        for (int j = 0; j < modelList.size(); j++) {
            MeasureDataModel modelNew = modelList.get(j);
            PolylineOptions lineOptions = new PolylineOptions();
            ArrayList<LatLng> points = new ArrayList();
            points.add(new LatLng(MyApplication.dmsToDouble(modelNew.getFrpLat()), MyApplication.dmsToDouble(modelNew.getFrpLong())));

            for (int i = 0; i < modelNew.getMeasureList().size(); i++) {
                points.add(
                        new LatLng(modelNew.getMeasureList().get(i).getLatLng().getLatitude(),
                                modelNew.getMeasureList().get(i).getLatLng().getLongitude()));
            }
            lineOptions.addAll(points);
            lineOptions.width(5);
            lineOptions.color(Color.parseColor("#FF5E00"));
            lineOptions.geodesic(true);
// Drawing polyline in the Google Map for the i-th route
            Polyline polyline = mMap.addPolyline(lineOptions);
            polylines.add(polyline);

        }

//        LatLngBounds.Builder builder = new LatLngBounds.Builder();


        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int j = 0; j < polylines.size(); j++) {
            for (int i = 0; i < polylines.get(j).getPoints().size(); i++) {
                builder.include(polylines.get(j).getPoints().get(i));
            }
        }
        LatLngBounds bounds = builder.build();
/*
        for (int i = 0; i < points.size(); i++) {
            if (i == 0)
                mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(textAsBitmap(model.getMeasureList().get(i).getPointFrom())))
                        .position(points.get(i))
                        .title(model.getMeasureList().get(i).getPointFrom()));
            else
                mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(textAsBitmap(model.getMeasureList().get(i - 1).getPointTo())))
                        .position(points.get(i))
                        .title(model.getMeasureList().get(i - 1).getPointTo()));

        }*/

        int padding = (int) (width * 0.12);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.animateCamera(cu);
    }

    public Bitmap textAsBitmap(String text) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(50.f);
        paint.setColor(Color.RED);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);

        int paddingX;
        int paddingY;

        if (image.getWidth() == image.getHeight()) {
            paddingX = 0;
            paddingY = 0;
        } else if (image.getWidth() > image.getHeight()) {
            paddingX = 0;
            paddingY = image.getWidth() - image.getHeight();
        } else {
            paddingX = image.getHeight() - image.getWidth();
            paddingY = 0;
        }

        Bitmap paddedBitmap = Bitmap.createBitmap(
                image.getWidth() + paddingX,
                image.getHeight() + paddingY,
                Bitmap.Config.ARGB_8888);

        canvas = new Canvas(paddedBitmap);


        canvas.drawARGB(0xFF, 0xFF, 0xFF, 0xFF); // this represents white color
        canvas.drawBitmap(
                image,
                paddingX / 2,
                paddingY / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

        return paddedBitmap;
    }

    public Bitmap createPolylineBitmap(Bitmap bitmap, ArrayList<Point> list) {

        String s = model.toString();

//        Bitmap bitmap = Bitmap.createBitmap(mapFragment.getView().getMeasuredWidth(), mapFragment.getView().getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        int minX = bitmap.getWidth();
        int minY = bitmap.getHeight();
        int maxX = -1;
        int maxY = -1;

        canvas.drawARGB(0xFF, 0xFF, 0xFF, 0xFF);
        canvas.translate((float) (minX / 2), (float) ((minY - 160) / 2));
        canvas.save();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#FD0000"));
        paint.setStrokeWidth(1);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        // paint.setPathEffect(new CornerPathEffect(10));
        paint.setAntiAlias(true);
        paint.setTextSize(8);

        for (int i = 0; i < list.size() - 1; i++) {
            Point point1 = list.get(i);
            Point point2 = list.get(i + 1);
            canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
            if (i == 0) {
            }
//                canvas.drawText(model.getMeasureList().get(i).getPointFrom(), point1.x, point1.y, paint);
            else

                canvas.drawText(model.getMeasureList().get(i - 1).getPointTo(), point1.x, point1.y, paint);
        }


        for (int y = 0; y < bitmap.getHeight(); y++) {
            for (int x = 0; x < bitmap.getWidth(); x++) {
                Log.d("alpha_trans", bitmap.getPixel(x, y) + "");
                int alpha = (bitmap.getPixel(x, y) >> 24) & 255;
                if (alpha > 0)   // pixel is not 100% transparent
                {
                    if (x < minX)
                        minX = x;
                    if (x > maxX)
                        maxX = x;
                    if (y < minY)
                        minY = y;
                    if (y > maxY)
                        maxY = y;
                }
            }
        }
        if ((maxX < minX) || (maxY < minY))
            return null; // Bitmap is entirely transparent
        Matrix matrix = new Matrix();

        matrix.postRotate(270);
        north(canvas, 0, 0, paint);
        Bitmap result = Bitmap.createBitmap(bitmap, minX, minY, (maxX - minX) + 1, (maxY - minY) + 1);
        return removeMargins(result, Color.WHITE);
    }

    public void north(Canvas canvas, int x, int y, Paint paint) {
        canvas.drawLine(x, y + 50, x, y - 50, paint);
        canvas.drawLine(x, y - 50, x + 5, y - 45, paint);
        canvas.drawLine(x, y - 50, x - 5, y - 45, paint);
        canvas.drawCircle(x, y, 5, paint);
       /* canvas.drawLine(x - 100, y - 180, x + 20, y - 180, paint);
        canvas.drawLine(x, y + 100, x - 5.0f, y - 80.0f, paint);
        canvas.drawLine(x, y + 100, x + 5.0f, -80.0f, paint);
        paint.setTextSize(10.0f);*/
        paint.setStyle(Paint.Style.FILL);
        canvas.drawText("N", x, y - 60, paint);
        Log.d("areanew", SphericalUtil.computeArea(model.getClosedArea()) + "");


        canvas.drawText("Area : " + result_unit + " " + getResources().getString(R.string.test_string), x, y + 120, paint);
        /*ArrayList<Double> latd = new ArrayList<>();
        ArrayList<Double> lngd = new ArrayList<>();
        latd.clear();
        lngd.clear();
        for (int i = 0; i < model.getMeasureList().size(); i++) {
            latd.add(model.getMeasureList().get(i).getLatLng().getLatitude());
            lngd.add(model.getMeasureList().get(i).getLatLng().getLongitude());


        }

        Log.d("calculate area", area(latd, lngd) + "");*/
    }


    private ArrayList<Point> getPointList() {
        ArrayList<Point> list = new ArrayList<>();
        /*for (int i = 0; i < polyline.getPoints().size(); i++) {
            list.add(LatLngToPoint(polyline.getPoints().get(i)));
        }*/
        list.add(new Point(0, 0));
        for (int i = 0; i < model.getMeasureList().size(); i++) {
            double distance = Float.parseFloat(model.getMeasureList().get(i).getDistance());
            double angle = Math.toRadians(MyApplication.dmsToDouble(model.getMeasureList().get(i).getBearing()) + 270.0d);
            Log.d("bearing", angle + "");
//            double angle = (MyApplication.dmsToDouble(model.getMeasureList().get(i).getBearing()));

            Double x, y;

            if (getMaxDist() > 300 && getMaxDist() < 601) {
                x = list.get(list.size() - 1).x + (distance / 2) * Math.cos(angle);
                y = list.get(list.size() - 1).y + (distance / 2) * Math.sin(angle);
            } else if (getMaxDist() > 600) {
                x = list.get(list.size() - 1).x + (distance / 3) * Math.cos(angle);
                y = list.get(list.size() - 1).y + (distance / 3) * Math.sin(angle);
            } else {
                x = list.get(list.size() - 1).x + (distance) * Math.cos(angle);
                y = list.get(list.size() - 1).y + (distance) * Math.sin(angle);
            }

            list.add(new Point(x.intValue(), y.intValue()));
        }
        return list;
    }

    private float getMaxDist() {
        float max = 0;
        for (int i = 0; i < model.getMeasureList().size(); i++) {
            if (max < Float.parseFloat(model.getMeasureList().get(i).getDistance())) {
                max = Float.parseFloat(model.getMeasureList().get(i).getDistance());
            }
        }
        return max;
    }

    @OnClick({R.id.save, R.id.img_back, R.id.img_delete})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save:
                MyApplication.initialize_progressbar(MapsActivity.this);
                mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        MyApplication.stop_progressbar();
                        MyApplication.bitmapMap = bitmap;
                        MyApplication.activityStart(MapsActivity.this, ReportActivity.class, false);
                    }
                });
                break;

            case R.id.img_back:
                markers.get(markers.size() - 1).remove();
                markers.remove(markers.size() - 1);
                updatePolygon();
                break;
            case R.id.img_delete:
                for (int i = 0; i < markers.size(); i++) {
                    markers.get(i).remove();
                }
                polyline.remove();
                markers = new ArrayList<>();
                llMeasure.setVisibility(View.GONE);
                break;
        }
    }


    private void calculateBearingDistance(com.javadocmd.simplelatlng.LatLng firstLatLng,
                                          com.javadocmd.simplelatlng.LatLng secondLatLng) {
        double bearing = 0d, distance = 0d;

        bearing = LatLngTool.initialBearing(firstLatLng, secondLatLng);
        distance = LatLngTool.distance(firstLatLng, secondLatLng, LengthUnit.METER);

        txtBearing.setText("Bearing: " + MyApplication.doubleToDmsNew(bearing));
        txtDistance.setText("Distance: " + BigDecimal.valueOf(distance).setScale(6, RoundingMode.HALF_UP).doubleValue() + " M");
        txtArea.setText("Area: " + BigDecimal.valueOf(SphericalUtil.computeArea(polyline.getPoints())).setScale(6, RoundingMode.HALF_UP).doubleValue() + " " + getResources().getString(R.string.test_string));
        txtPerimeter.setText("Perimeter: " + BigDecimal.valueOf(SphericalUtil.computeLength(polyline.getPoints())).setScale(6, RoundingMode.HALF_UP).doubleValue() + " M");

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        updatePolygon();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    private class CreateBitmap extends AsyncTask<Void, Void, Bitmap> {
        Bitmap bitmap = null;
        ArrayList<Point> list = new ArrayList<>();

        public CreateBitmap(Bitmap bitmap, ArrayList<Point> list) {
            this.bitmap = bitmap;
            this.list = list;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            MyApplication.bitmap = result;
            MyApplication.stop_progressbar();

            imgDrawing.setImageBitmap(result);
        }

        @Override
        protected Bitmap doInBackground(Void... params) {

            return createPolylineBitmap(bitmap, list);
        }

        @Override
        protected void onPreExecute() {
            MyApplication.initialize_progressbar(MapsActivity.this);
            calculateArea();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private double area(ArrayList<Double> lats, ArrayList<Double> lons) {
        double sum = 0;
        double prevcolat = 0;
        double prevaz = 0;
        double colat0 = 0;
        double az0 = 0;
        for (int i = 0; i < lats.size(); i++) {
            double colat = 2 * Math.atan2(Math.sqrt(Math.pow(Math.sin(lats.get(i) * Math.PI / 180 / 2), 2) + Math.cos(lats.get(i) * Math.PI / 180) * Math.pow(Math.sin(lons.get(i) * Math.PI / 180 / 2), 2)), Math.sqrt(1 - Math.pow(Math.sin(lats.get(i) * Math.PI / 180 / 2), 2) - Math.cos(lats.get(i) * Math.PI / 180) * Math.pow(Math.sin(lons.get(i) * Math.PI / 180 / 2), 2)));
            double az = 0;
            if (lats.get(i) >= 90) {
                az = 0;
            } else if (lats.get(i) <= -90) {
                az = Math.PI;
            } else {
                az = Math.atan2(Math.cos(lats.get(i) * Math.PI / 180) * Math.sin(lons.get(i) * Math.PI / 180), Math.sin(lats.get(i) * Math.PI / 180)) % (2 * Math.PI);
            }
            if (i == 0) {
                colat0 = colat;
                az0 = az;
            }
            if (i > 0 && i < lats.size()) {
                sum = sum + (1 - Math.cos(prevcolat + (colat - prevcolat) / 2)) * Math.PI * ((Math.abs(az - prevaz) / Math.PI) - 2 * Math.ceil(((Math.abs(az - prevaz) / Math.PI) - 1) / 2)) * Math.signum(az - prevaz);
            }
            prevcolat = colat;
            prevaz = az;
        }
        sum = sum + (1 - Math.cos(prevcolat + (colat0 - prevcolat) / 2)) * (az0 - prevaz);
        return 5.10072E14 * Math.min(Math.abs(sum) / 4 / Math.PI, 1 - Math.abs(sum) / 4 / Math.PI);
    }

    public void calculateArea() {
        Object obj;
        Object obj2 = null;
        double d;

        // this.f3009l = (TextView) findViewById(R.id.tv_out);
        String str = "";
        Log.d("array size", model.getMeasureList().size() + "");
        if (model.getMeasureList().size() > 2) {
            int i;
            double[] dArr = new double[model.getMeasureList().size()];
            double[] dArr2 = new double[model.getMeasureList().size()];
            double d2 = 0.0d;
            double d3 = 0.0d;
            Object obj3 = null;
            int i2 = 1;
            int dataget = 0;
            while (i2 <= model.getMeasureList().size()) {
                // EditText editText = (EditText) findViewById(i2 * 2);
                // EditText editText2 = (EditText) findViewById((i2 * 2) + 1);
                String lat_str = MyApplication.doubleToDmslat(model.getMeasureList().get(dataget).getLatLng().getLatitude());
                String long_str = MyApplication.doubleToDmsLong(model.getMeasureList().get(dataget).getLatLng().getLongitude());
                Log.d("lattitude_str", MyApplication.doubleToDmslat(model.getMeasureList().get(dataget).getLatLng().getLatitude()));
                dataget++;
                if (lat_str.length() <= 0 || long_str.length() <= 0) {
                    obj = 1;
                } else {
                    String[] split = lat_str.split("\\s+");
                    String[] split2 = long_str.split("\\s+");
                    if (split.length < 2 || split2.length < 2) {
                        obj = 1;
                    } else {
                        try {
                            String toUpperCase;
                            if (split.length == 2) {
                                toUpperCase = split[1].trim().toUpperCase();
                                obj2 = -1;
                                switch (toUpperCase.hashCode()) {
                                    case 78:
                                        if (toUpperCase.equals("N")) {
                                            obj2 = null;
                                            break;
                                        }
                                        break;
                                    case 83:
                                        if (toUpperCase.equals("S")) {
                                            obj2 = 1;
                                            break;
                                        }
                                        break;
                                }
                                if (obj2 == null) {
                                    d2 = Double.parseDouble(split[0].trim());
                                } else if ((int) obj2 == 1) {
                                    d2 = Double.parseDouble(split[0].trim()) * -1.0d;
                                } else {
                                    obj3 = 1;
                                }

                                /*switch (obj2) {
                                    case null:
                                        d2 = Double.parseDouble(split[0].trim());
                                        break;
                                    case 1:
                                        d2 = Double.parseDouble(split[0].trim()) * -1.0d;
                                        break;
                                    default:
                                        obj3 = 1;
                                        break;
                                }*/
                                obj2 = obj3;
                            } else if (split.length == 3) {
                                toUpperCase = split[2].trim().toUpperCase();
                                obj2 = -1;
                                switch (toUpperCase.hashCode()) {
                                    case 78:
                                        if (toUpperCase.equals("N")) {
                                            obj2 = null;
                                            break;
                                        }
                                        break;
                                    case 83:
                                        if (toUpperCase.equals("S")) {
                                            obj2 = 1;
                                            break;
                                        }
                                        break;
                                }
                                if (obj2 == null) {
                                    d2 = (Double.parseDouble(split[1].trim()) / 60.0d) + ((double) Integer.parseInt(split[0].trim()));
                                } else if ((int) obj2 == 1) {
                                    d2 = ((Double.parseDouble(split[1].trim()) / 60.0d) + ((double) Integer.parseInt(split[0].trim()))) * -1.0d;
                                } else {
                                    obj3 = 1;
                                }

                               /* switch (obj2) {
                                    case null:
                                        d2 = (Double.parseDouble(split[1].trim()) / 60.0d) + ((double) Integer.parseInt(split[0].trim()));
                                        break;
                                    case 1:
                                        d2 = ((Double.parseDouble(split[1].trim()) / 60.0d) + ((double) Integer.parseInt(split[0].trim()))) * -1.0d;
                                        break;
                                    default:
                                        obj3 = 1;
                                        break;
                                }*/
                                obj2 = obj3;
                            } else if (split.length == 4) {
                                toUpperCase = split[3].trim().toUpperCase();
                                obj2 = -1;
                                switch (toUpperCase.hashCode()) {
                                    case 78:
                                        if (toUpperCase.equals("N")) {
                                            obj2 = null;
                                            break;
                                        }
                                        break;
                                    case 83:
                                        if (toUpperCase.equals("S")) {
                                            obj2 = 1;
                                            break;
                                        }
                                        break;
                                }
                                if (obj2 == null) {
                                    d2 = (((Double.parseDouble(split[2].trim()) / 60.0d) + ((double) Integer.parseInt(split[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split[0].trim()));
                                    obj2 = obj3;
                                } else if ((int) obj2 == 1) {
                                    d2 = ((((Double.parseDouble(split[2].trim()) / 60.0d) + ((double) Integer.parseInt(split[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split[0].trim()))) * -1.0d;
                                    obj2 = obj3;
                                } else {
                                    i = 1;
                                }

                               /* switch (obj2) {
                                    case null:
                                        d2 = (((Double.parseDouble(split[2].trim()) / 60.0d) + ((double) Integer.parseInt(split[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split[0].trim()));
                                        obj2 = obj3;
                                        break;
                                    case 1:
                                        d2 = ((((Double.parseDouble(split[2].trim()) / 60.0d) + ((double) Integer.parseInt(split[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split[0].trim()))) * -1.0d;
                                        obj2 = obj3;
                                        break;
                                    default:
                                        i = 1;
                                        break;
                                }*/
                            } else {
                                obj2 = obj3;
                            }
                            String toUpperCase2;
                            if (split2.length != 2) {
                                if (split2.length != 3) {
                                    if (split2.length == 4) {
                                        toUpperCase2 = split2[3].trim().toUpperCase();
                                        obj = -1;
                                        switch (toUpperCase2.hashCode()) {
                                            case 69:
                                                if (toUpperCase2.equals("E")) {
                                                    obj = null;
                                                    break;
                                                }
                                                break;
                                            case 87:
                                                if (toUpperCase2.equals("W")) {
                                                    obj = 1;
                                                    break;
                                                }
                                                break;
                                        }
                                        if (obj == null) {
                                            d3 = (((Double.parseDouble(split2[2].trim()) / 60.0d) + ((double) Integer.parseInt(split2[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()));
                                        } else if ((int) obj == 1) {
                                            d3 = ((((Double.parseDouble(split2[2].trim()) / 60.0d) + ((double) Integer.parseInt(split2[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()))) * -1.0d;
                                        } else {
                                            obj2 = 1;
                                        }

                                       /* switch (obj) {
                                            case null:
                                                d3 = (((Double.parseDouble(split2[2].trim()) / 60.0d) + ((double) Integer.parseInt(split2[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()));
                                                break;
                                            case 1:
                                                d3 = ((((Double.parseDouble(split2[2].trim()) / 60.0d) + ((double) Integer.parseInt(split2[1].trim()))) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()))) * -1.0d;
                                                break;
                                            default:
                                                obj2 = 1;
                                                break;
                                        }*/
                                    }
                                }
                               /* toUpperCase2 = split2[2].trim().toUpperCase();
                                obj = -1;
                                switch (toUpperCase2.hashCode()) {
                                    case 69:
                                        if (toUpperCase2.equals("E")) {
                                            obj = null;
                                            break;
                                        }
                                        break;
                                    case 87:
                                        if (toUpperCase2.equals("W")) {
                                            obj = 1;
                                            break;
                                        }
                                        break;
                                }
                                if (obj==null){
                                    d3 = (Double.parseDouble(split2[1].trim()) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()));
                                }else if ((int)obj==1){
                                    d3 = ((Double.parseDouble(split2[1].trim()) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()))) * -1.0d;
                                }else {
                                    obj2 = 1;
                                }
*/
                               /* switch (obj) {
                                    case null:
                                        d3 = (Double.parseDouble(split2[1].trim()) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()));
                                        break;
                                    case 1:
                                        d3 = ((Double.parseDouble(split2[1].trim()) / 60.0d) + ((double) Integer.parseInt(split2[0].trim()))) * -1.0d;
                                        break;
                                    default:
                                        obj2 = 1;
                                        break;
                                }*/
                            }
                           /* toUpperCase2 = split2[1].trim().toUpperCase();
                            obj = -1;
                            switch (toUpperCase2.hashCode()) {
                                case 69:
                                    if (toUpperCase2.equals("E")) {
                                        obj = null;
                                        break;
                                    }
                                    break;
                                case 87:
                                    if (toUpperCase2.equals("W")) {
                                        obj = 1;
                                        break;
                                    }
                                    break;
                            }

                            if (obj==null){
                                d3 = Double.parseDouble(split2[0].trim());
                            }else if ((int)obj==1){
                                d3 = Double.parseDouble(split2[0].trim()) * -1.0d;
                            }else {
                                obj2 = 1;
                            }*/
                            /*switch (obj) {
                                case null:
                                    d3 = Double.parseDouble(split2[0].trim());
                                    break;
                                case 1:
                                    d3 = Double.parseDouble(split2[0].trim()) * -1.0d;
                                    break;
                                default:
                                    obj2 = 1;
                                    break;
                            }*/
                        } catch (NumberFormatException e) {
                            d = d2;
                            d2 = d;
                            i = 1;
                            dArr[i2 - 1] = d3;
                            dArr2[i2 - 1] = d2;
                            obj = obj2;
                            i2++;
                            obj3 = obj;
                        } catch (NullPointerException e2) {
                            d = d2;
                            d2 = d;
                            i = 1;
                            dArr[i2 - 1] = d3;
                            dArr2[i2 - 1] = d2;
                            obj = obj2;
                            i2++;
                            obj3 = obj;
                        } catch (ArrayIndexOutOfBoundsException e3) {
                            d = d2;
                            d2 = d;
                            i = 1;
                            dArr[i2 - 1] = d3;
                            dArr2[i2 - 1] = d2;
                            obj = obj2;
                            i2++;
                            obj3 = obj;
                        }
                        dArr[i2 - 1] = d3;
                        dArr2[i2 - 1] = d2;
                        obj = obj2;
                    }
                }
                i2++;
                obj3 = obj;
            }
            if (obj3 != null) {
                Toast.makeText(getApplicationContext(), "Check the inputs!", Toast.LENGTH_SHORT).show();
                str = "";
               /* if (Build.VERSION.SDK_INT >= 24) {
                    this.f3009l.setText(Html.fromHtml(str, 0));
                    return;
                } else {
                    this.f3009l.setText(Html.fromHtml(str));
                    return;
                }*/
                Log.d("area calculation", str);
            }
            double[] dArr3 = new double[((model.getMeasureList().size() - 1) * 2)];
            for (i = 0; i < dArr.length - 1; i++) {
                double[] a = m4828a(dArr2[i], dArr[i], dArr2[i + 1], dArr[i + 1]);
                dArr3[i * 2] = a[0];
                dArr3[(i * 2) + 1] = a[1];
            }
            double[] dArr4 = new double[model.getMeasureList().size()];
            double[] dArr5 = new double[model.getMeasureList().size()];
            double d4 = 0.0d;
            double d5 = 0.0d;
            double d6 = 0.0d;
            d2 = 0.0d;
            d3 = 0.0d;
            double d7 = 0.0d;
            dArr4[0] = 0.0d;
            dArr5[0] = 0.0d;
            for (i = 0; i < dArr3.length / 2; i++) {
                double d8 = dArr3[i * 2];
                double d9 = dArr3[(i * 2) + 1];
                if (d9 < 90.0d) {
                    d4 += Math.cos(Math.toRadians(90.0d - d9)) * d8;
                    d5 += d8 * Math.sin(Math.toRadians(90.0d - d9));
                } else if (d9 < 180.0d) {
                    d4 += Math.cos(Math.toRadians(d9 - 90.0d)) * d8;
                    d5 += (d8 * -1.0d) * Math.sin(Math.toRadians(d9 - 90.0d));
                } else if (d9 < 270.0d) {
                    d4 += (-1.0d * d8) * Math.cos(Math.toRadians(270.0d - d9));
                    d5 += (d8 * -1.0d) * Math.sin(Math.toRadians(270.0d - d9));
                } else if (d9 < 360.0d) {
                    d4 += (-1.0d * d8) * Math.cos(Math.toRadians(d9 - 270.0d));
                    d5 += d8 * Math.sin(Math.toRadians(d9 - 270.0d));
                }
                dArr4[i + 1] = d4;
                dArr5[i + 1] = d5;
                if (dArr4[i + 1] < d6) {
                    d6 = dArr4[i + 1];
                }
                if (dArr4[i + 1] > d2) {
                    d2 = dArr4[i + 1];
                }
                if (dArr5[i + 1] < d3) {
                    d3 = dArr5[i + 1];
                }
                if (dArr5[i + 1] > d7) {
                    d7 = dArr5[i + 1];
                }
            }
            i = 0;
            d5 = 0.0d;
            while (i < dArr4.length) {
                d5 += (dArr4[i] * dArr5[i + 1 < dArr5.length ? i + 1 : 0]) - (dArr4[i + 1 < dArr4.length ? i + 1 : 0] * dArr5[i]);
                i++;
            }
            d = Math.abs(d5 / 2.0d);
            str = "<font color=#795548>" + "Area" + ": </font>" + Double.toString(((double) Math.round((d / 4046.8564224d) * 10000.0d)) / 10000.0d) + " " + getResources().getString(R.string.input_area_acre).toLowerCase() + "s = " + Double.toString(((double) Math.round((d / 10000.0d) * 10000.0d)) / 10000.0d) + " " + getResources().getString(R.string.input_area_acre).toLowerCase() + "s";

            result_unit = Double.toString((((double) Math.round((d / 4046.8564224d) * 10000.0d)) / 10000.0d) * 4046.86);
            Log.d("actual result", result_unit);
            //  Double df=(((double) Math.round((d / 4046.8564224d) * 10000.0d)) / 10000.0d)*4046.86;

            /*if (Build.VERSION.SDK_INT >= 24) {
                this.f3009l.setText(Html.fromHtml(str, 0));
            } else {
                this.f3009l.setText(Html.fromHtml(str));
            }*/
            Log.d("area calculation", str);
           /* int right = this.drawPlot.getRight() - this.drawPlot.getLeft();
            this.drawPlot.setMinimumHeight(right);
            d2 -= d6;
            d3 = d7 - d3;
            float f = (float) (((double) right) / (d2 > d3 ? 1.2d * d2 : 1.2d * d3));
            for (i = 0; i < dArr4.length; i++) {
                if (dArr4[i] == d6) {
                    dArr4[i] = dArr4[i] + Math.abs(d6);
                } else {
                    dArr4[i] = dArr4[i] + Math.abs(d6);
                    dArr4[i] = dArr4[i] * ((double) f);
                }
                if (dArr5[i] == d7) {
                    dArr5[i] = dArr5[i] - d7;
                } else {
                    dArr5[i] = dArr5[i] - d7;
                    dArr5[i] = dArr5[i] * ((double) f);
                }
            }
            if (d3 > d2) {
                d7 = (((double) right) - (((double) f) * d2)) / 2.0d;
                for (i = 0; i < dArr4.length; i++) {
                    dArr4[i] = dArr4[i] + d7;
                }
            }
            float[] fArr = new float[dArr4.length];
            float[] fArr2 = new float[dArr4.length];
            for (i = 0; i < dArr4.length; i++) {
                fArr[i] = (float) dArr4[i];
                fArr2[i] = (float) dArr5[i];
                Log.d("xarray",fArr[i]+"");
                Log.d("yarray",fArr2[i]+"");
            }


            this.drawPlot.m4836a(fArr, fArr2);*/
            return;
        }
       /* str = "<font color=#e53935>" + getResources().getString(R.string.txt_error_ll) + "</font>";
        if (Build.VERSION.SDK_INT >= 24) {
            this.f3009l.setText(Html.fromHtml(str, 0));
        } else {
            this.f3009l.setText(Html.fromHtml(str));
        }*/
    }

    private double[] m4828a(double d, double d2, double d3, double d4) {
        double toRadians = Math.toRadians(d3 - d);
        double toRadians2 = Math.toRadians(d4 - d2);
        double toRadians3 = Math.toRadians(d);
        double toRadians4 = Math.toRadians(d3);
        toRadians = (Math.sin(toRadians / 2.0d) * Math.sin(toRadians / 2.0d)) + (((Math.cos(toRadians3) * Math.cos(toRadians4)) * Math.sin(toRadians2 / 2.0d)) * Math.sin(toRadians2 / 2.0d));
        double atan2 = ((double) 6371000) * (Math.atan2(Math.sqrt(toRadians), Math.sqrt(1.0d - toRadians)) * 2.0d);
        toRadians = (Math.toDegrees(Math.atan2(Math.sin(toRadians2) * Math.cos(toRadians4), (Math.cos(toRadians3) * Math.sin(toRadians4)) - (Math.cos(toRadians2) * (Math.sin(toRadians3) * Math.cos(toRadians4))))) + 360.0d) % 360.0d;
        return new double[]{atan2, toRadians};
    }


}
