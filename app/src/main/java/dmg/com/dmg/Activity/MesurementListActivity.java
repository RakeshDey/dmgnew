package dmg.com.dmg.Activity;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import dmg.com.dmg.Adapter.MesurementAdapter;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Model.MeasureDataModel;
import dmg.com.dmg.Model.MesurementBinder;
import dmg.com.dmg.Network.ApiService;
import dmg.com.dmg.R;

/**
 * Created by Advosoft2 on 4/6/2018.
 */

public class MesurementListActivity extends AppCompatActivity {
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.empty_view)
    TextView emptyView;
    private static final int VERTICAL_ITEM_SPACE = 5;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    ArrayList<MesurementBinder> data_arr=new ArrayList<>();
    MesurementAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mesurement);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle("Saved List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callApi();
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void callApi() {

         ApiService.getInstance().makeGetCall(MesurementListActivity.this,ApiService.BASE_URL+ ApiService.GET_List+"?access_token="+ MyApplication.getSharedPrefString("userId"), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                try{
                    Log.d("response",response.toString());
                    JSONArray jsonArray=response.getJSONObject("success").getJSONArray("data");
                    JSONObject jobj=null;
                    data_arr.clear();
                    for (int i=0;i<jsonArray.length();i++){
                        jobj=jsonArray.getJSONObject(i);
                        MesurementBinder binder=new MesurementBinder();
                        binder.setMl_no(jobj.optString("ml_no"));
                        binder.setMl_name(jobj.optString("ml_name"));
                        binder.setYear(jobj.optString("year"));
                        binder.setFrp_name(jobj.optString("frp_name"));
                        binder.setAddress(jobj.optString("address"));
                        binder.setPdf(jobj.optString("pdf"));
                        data_arr.add(binder);


                    }
                    setList();

                }catch (Exception e){

                }


            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setList(){
        if (data_arr.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapter = new MesurementAdapter(MesurementListActivity.this, data_arr);
            recycleView.setAdapter(adapter);


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
          if (id == android.R.id.home) {
            MyApplication.activityFinish(MesurementListActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }
}
