package dmg.com.dmg.Activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.R;

public class MeasurementActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.edt_lat1)
    EditText edtLat1;
    @Bind(R.id.edt_lng1)
    EditText edtLng1;
    @Bind(R.id.edt_lat2)
    EditText edtLat2;
    @Bind(R.id.edt_lng2)
    EditText edtLng2;
    @Bind(R.id.edt_bearing)
    EditText edtBearing;
    @Bind(R.id.edt_distance)
    EditText edtDistance;
    @Bind(R.id.btn_start)
    TextView btnStart;
    @Bind(R.id.spn_type)
    Spinner spnType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setTitle("Measurement");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    edtLat2.setEnabled(false);
                    edtLng2.setEnabled(false);
                    edtBearing.setEnabled(true);
                    edtDistance.setEnabled(true);
                } else if (position == 1) {
                    edtLat2.setEnabled(true);
                    edtLng2.setEnabled(true);
                    edtBearing.setEnabled(false);
                    edtDistance.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtLat1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    String newString = s.toString();
                    String c = s.charAt(s.length() - 1) + "";
                    if (s.length() > 1 && !s.toString().startsWith(" ")) {


                        if (c.equalsIgnoreCase(" ")) {
                            if (!newString.contains("°")) {
                                newString = newString.replace(" ", "°");
                            } else if (!newString.contains("'")) {
                                newString = newString.replace(" ", "'");
                            } else if (!newString.contains("\"")) {
                                newString = newString.replace(" ", "\"");
                            } else {
                                newString = newString.replace(" ", "");
                            }
                            edtLat1.setText(newString);
                            edtLat1.setSelection(newString.length());
                        }
                    } else if (s.toString().startsWith(" ")) {
                        newString = newString.replace(" ", "");
                        edtLat1.setText(newString);
                        edtLat1.setSelection(newString.length());
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void calculateBearingDistance() {
        LatLng firstLatLng = null, secondLatLng = null;
        double bearing = 0d, distance = 0d;
        try {
            firstLatLng = new LatLng(MyApplication.dmsToDouble(edtLat1.getText().toString()),
                    MyApplication.dmsToDouble(edtLng1.getText().toString()));
            secondLatLng = new LatLng(MyApplication.dmsToDouble(edtLat2.getText().toString()),
                    MyApplication.dmsToDouble(edtLng2.getText().toString()));

        } catch (NumberFormatException e) {
            MyApplication.showToast(MeasurementActivity.this, "Enter valid data");
            return;
        }


        bearing = LatLngTool.initialBearing(firstLatLng, secondLatLng);
        distance = LatLngTool.distance(firstLatLng, secondLatLng, LengthUnit.METER);

        edtBearing.setText(MyApplication.doubleToDms(bearing));
        edtDistance.setText(Math.floor(distance) + "");


        edtBearing.setEnabled(false);
        edtDistance.setEnabled(false);
        edtLat2.setEnabled(true);
        edtLng2.setEnabled(true);
    }

    private void calculateLatLng() {
        LatLng firstLatLng = null, secondLatLng;
        double bearing = 0d, distance = 0d;
        try {
            firstLatLng = new LatLng(MyApplication.dmsToDouble(edtLat1.getText().toString()),
                    MyApplication.dmsToDouble(edtLng1.getText().toString()));
            bearing = MyApplication.dmsToDouble(edtBearing.getText().toString());
            distance = Double.parseDouble(edtDistance.getText().toString());
        } catch (NumberFormatException e) {
            MyApplication.showToast(MeasurementActivity.this, "Enter valid data");
            return;
        }

        secondLatLng = LatLngTool.travel(firstLatLng, bearing, distance, LengthUnit.METER);
        edtLat2.setText(MyApplication.doubleToDms(secondLatLng.getLatitude()));
        edtLng2.setText(MyApplication.doubleToDms(secondLatLng.getLongitude()));

        edtLat2.setEnabled(false);
        edtLng2.setEnabled(false);
        edtBearing.setEnabled(true);
        edtDistance.setEnabled(true);
    }

    @OnClick(R.id.btn_start)
    public void onClick() {
        if (spnType.getSelectedItemPosition() == 0) {
            calculateLatLng();
        } else if (spnType.getSelectedItemPosition() == 1) {
            calculateBearingDistance();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(MeasurementActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }
}
