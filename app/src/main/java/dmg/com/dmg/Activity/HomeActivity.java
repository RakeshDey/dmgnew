package dmg.com.dmg.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dmg.com.dmg.Adapter.DrawerListAdapter;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Custom.CircularImageView;
import dmg.com.dmg.Network.ApiService;
import dmg.com.dmg.R;
import dmg.com.dmg.UI.FreMap;

public class HomeActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.imageView)
    CircularImageView imageView;
    @Bind(R.id.text_name)
    TextView textName;
    @Bind(R.id.lv_drawer)
    ListView lvDrawer;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    DrawerListAdapter drawerListAdapter;
    @Bind(R.id.contentContainer)
    FrameLayout contentContainer;
    FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Home");
        checkVerify();

        setDrawer();
        changeFragment(new FreMap());
    }

    private void setDrawer() {
        textName.setText(MyApplication.getSharedPrefString("name"));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

            }
        };

        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        drawerListAdapter = new DrawerListAdapter(HomeActivity.this);
        drawerListAdapter.addMenu("Measurement");
        drawerListAdapter.addMenu("Saved Measure");
        drawerListAdapter.addMenu("Group");

        drawerListAdapter.addMenu("Import");
        drawerListAdapter.addMenu("Export");
        drawerListAdapter.addMenu("Help");
        drawerListAdapter.addMenu("Contact Us");
        drawerListAdapter.addMenu("Logout");
        lvDrawer.setAdapter(drawerListAdapter);

        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drawerListAdapter.setSelectedPosition(position);
                if (position == 0) {
                    MyApplication.activityStart(HomeActivity.this, MeasurementActivity.class, false);
                } else if (position == 1) {
                    MyApplication.activityStart(HomeActivity.this, MesurementListActivity.class, false);
                } else if (position == 2) {
                    MyApplication.showToast(HomeActivity.this, "Coming Soon");
                } else if (position == 3) {
                    MyApplication.showToast(HomeActivity.this, "Coming Soon");
                } else if (position == 4) {
                    MyApplication.showToast(HomeActivity.this, "Coming Soon");
                } else if (position == 5) {
                    MyApplication.showToast(HomeActivity.this, "Coming Soon");
                } else if (position == 6) {
                    MyApplication.showToast(HomeActivity.this, "Coming Soon");
                } else if (position == 7) {
                    MyApplication.setSharedPrefString("name", "");
                    MyApplication.setSharedPrefString("userId", "");
                    MyApplication.activityStart(HomeActivity.this, LogInActivity.class, true);
                }

            }
        });

    }

    public void changeFragment(Fragment fragment) {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentContainer, fragment);
        transaction.commit();
    }

    private void checkVerify() {
        ApiService.getInstance().makeGetCall(HomeActivity.this, ApiService.VERIFY_USER + "?peopleId=" + MyApplication.getSharedPrefString("peopleId"), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                try {
                    Log.d("response", response + "");
                    if (response.getJSONObject("success").getJSONObject("data").getBoolean("status") == false) {

                        MyApplication.setSharedPrefString("name", "");
                        MyApplication.setSharedPrefString("userId", "");
                        MyApplication.activityStart(HomeActivity.this, LogInActivity.class, true);
                        Toast.makeText(HomeActivity.this, response.getJSONObject("success").optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }


}
