package dmg.com.dmg.Activity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Model.UserModel;
import dmg.com.dmg.Network.ApiService;
import dmg.com.dmg.R;

public class LogInActivity extends AppCompatActivity {
    HorizontalScrollView horizontalScrollView;
    Bitmap bitmap;
    @Bind(R.id.edt_user_name)
    EditText edtUserName;
    @Bind(R.id.edt_password)
    EditText edtPassword;
    @Bind(R.id.btn_start)
    TextView btnStart;
    @Bind(R.id.ll)
    LinearLayout ll;
    @Bind(R.id.btn_signup)
    TextView btnSignup;
    @Bind(R.id.forgot_password)
    TextView forgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

    }


    @OnClick({R.id.btn_start, R.id.btn_signup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                if (isValid())
                    logIn();
                break;
            case R.id.btn_signup:
                MyApplication.activityStart(LogInActivity.this, SignUpActivity.class, false);
                break;

        }
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(edtPassword.getText().toString())) {
            MyApplication.setError(edtPassword, null);
            return false;
        }
        if (TextUtils.isEmpty(edtUserName.getText().toString())) {
            MyApplication.setError(edtUserName, null);
            return false;
        }
        if (!MyApplication.isEmailValid(edtUserName.getText().toString())) {
            MyApplication.setError(edtUserName, "Invalid Email address");
            return false;
        }

        return true;
    }

    private void logIn() {
        JSONObject param = new JSONObject();
        String deviceId = MyApplication.getDeviceId(LogInActivity.this).substring(7, 14);
        String pass = String.valueOf(Integer.parseInt(deviceId) / 5);

        try {
//            param.put("deviceId", "12345");
            param.put("deviceId", MyApplication.getDeviceId(LogInActivity.this));
            param.put("email", edtUserName.getText());
            param.put("password", edtPassword.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance().makePostCall(LogInActivity.this, ApiService.LOG_IN, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                UserModel userModel = MyApplication.gson.fromJson(response.optJSONObject("success").optJSONObject("user").toString(), UserModel.class);
                MyApplication.setSharedPrefString("name", userModel.getFullName());
                MyApplication.setSharedPrefString("userId", response.optJSONObject("success").optString("id"));
                MyApplication.setSharedPrefString("peopleId", userModel.getId());
                MyApplication.activityStart(LogInActivity.this, HomeActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    public void showDialog() {
        //Dialog dialog = new Dialog(context, android.R.style.Theme_Light); --for full screen
        final Dialog dialog = new Dialog(LogInActivity.this,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.forgot_password);
        final EditText edit_email = (EditText) dialog.findViewById(R.id.edit_email);
        TextView submit = (TextView) dialog.findViewById(R.id.txt_submit);
        TextView cancel = (TextView) dialog.findViewById(R.id.txt_resend);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_email.getText().toString())) {
                    MyApplication.setError(edit_email, null);

                } else {
                    dialog.dismiss();
                    apiForgot(edit_email);
                }
            }
        });


        dialog.show();

    }

    private void apiForgot(EditText emailtxt) {
        JSONObject param = new JSONObject();

        try {
//            param.put("deviceId", "12345");
            param.put("email", emailtxt.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance().makePostCall(LogInActivity.this, ApiService.FORGOT, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response",response+"");
                Toast.makeText(LogInActivity.this,response.optJSONObject("success").optString("msg"),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }


}
