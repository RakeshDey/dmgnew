package dmg.com.dmg.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import crl.android.pdfwriter.PDFWriter;
import crl.android.pdfwriter.PaperSize;
import crl.android.pdfwriter.StandardFonts;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Model.MeasureDataModel;
import dmg.com.dmg.Network.ApiConfig;
import dmg.com.dmg.Network.AppConfig;
import dmg.com.dmg.R;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class ReportActivity extends AppCompatActivity {

    @Bind(R.id.tlGridTable)
    TableLayout tlGridTable;
    MeasureDataModel model = new MeasureDataModel();
    @Bind(R.id.txt_from)
    TextView txtFrom;
    @Bind(R.id.horizontalView)
    HorizontalScrollView horizontalView;
    @Bind(R.id.txt_address)
    TextView txtAddress;
    @Bind(R.id.img_polyline)
    ImageView imgPolyline;
    Bitmap bitmap;
    @Bind(R.id.img_table)
    ImageView imgTable;
    @Bind(R.id.scroll)
    ScrollView scroll;
    @Bind(R.id.save)
    TextView save;
    @Bind(R.id.img_switch)
    ImageView imgSwitch;
    boolean isMap = false;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setTitle("Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        model = MyApplication.model;

        heading = "MAP SHOWING THE APPLIED " + model.getType() + " AREA " +
                model.getMlNo() + " FOR " + model.getMlName().toUpperCase() + " " + model.getFrpAddress().toUpperCase();

        txtAddress.setText(heading);
        imgPolyline.setImageBitmap(MyApplication.bitmap);
        for (int i = 0; i < model.getMeasureList().size(); i++)
            tlGridTable.addView(getView(model.getMeasureList().get(i)));


        horizontalView.post(new Runnable() {
            @Override
            public void run() {

                bitmap = MyApplication.getBitmapFromView(horizontalView.getChildAt(0));
                Toast.makeText(ReportActivity.this, "Done", Toast.LENGTH_LONG).show();
                horizontalView.setVisibility(View.GONE);
                imgTable.setImageBitmap(bitmap);
            }
        });

      /*  imgTable.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View arg0, MotionEvent event) {

                float curX, curY;
                float mx = 0.f, my = 0.f;

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        mx = event.getX();
                        my = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        curX = event.getX();
                        curY = event.getY();
                        imgTable.scrollBy((int) (mx - curX), (int) (my - curY));
                        mx = curX;
                        my = curY;
                        break;
                    case MotionEvent.ACTION_UP:
                        curX = event.getX();
                        curY = event.getY();
                        imgTable.scrollBy((int) (mx - curX), (int) (my - curY));
                        break;
                }

                return true;
            }
        });*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(ReportActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private View getView(MeasureDataModel.MeasureModel measureModel) {
        TableRow tableRow = new TableRow(ReportActivity.this);
        TextView from = getTextView();


        TextView to = getTextView();
        TextView bearing = getTextView();
        TextView distance = getTextView();
        TextView lat = getTextView();
        TextView lng = getTextView();


        from.setText(measureModel.getPointFrom());
        to.setText(measureModel.getPointTo());
        bearing.setText(measureModel.getBearing());
        bearing.setGravity(View.TEXT_ALIGNMENT_TEXT_END);
        distance.setText(measureModel.getDistance() + " M");
        lat.setText(MyApplication.doubleToDms(measureModel.getLatLng().getLatitude()));
        lng.setText(MyApplication.doubleToDms(measureModel.getLatLng().getLongitude()));
        lat.setGravity(View.TEXT_ALIGNMENT_TEXT_END);
        lng.setGravity(View.TEXT_ALIGNMENT_TEXT_END);
      /*  lat.setText(BigDecimal.valueOf(measureModel.getLatLng().getLatitude()).setScale(6, RoundingMode.HALF_UP).doubleValue() + "");
        lng.setText(BigDecimal.valueOf(measureModel.getLatLng().getLongitude()).setScale(6, RoundingMode.HALF_UP).doubleValue() + "");
*/

        tableRow.addView(from);
        tableRow.addView(to);
        tableRow.addView(bearing);
        tableRow.addView(distance);
        tableRow.addView(lat);
        tableRow.addView(lng);

        return tableRow;
    }

    private TextView getTextView() {
        TextView textView = new TextView(ReportActivity.this);
        textView.setLayoutParams(new TableRow.LayoutParams(1));
        textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextColor(Color.BLACK);
        textView.setBackgroundColor(Color.WHITE);
        textView.setPadding(txtFrom.getPaddingLeft(), txtFrom.getPaddingTop(), txtFrom.getPaddingRight(), txtFrom.getPaddingBottom());
        return textView;
    }

    private void generateHelloWorldPDF() {
        PDFWriter mPDFWriter = new PDFWriter(PaperSize.A4_WIDTH, PaperSize.A4_HEIGHT);
        mPDFWriter.setFont(StandardFonts.TIMES_BOLD, StandardFonts.TIMES_ROMAN);

        mPDFWriter.addText(10, 800, 15, "REVISE MAP SHOWING THE APPLIED" + model.getType() + "AREA ");
        mPDFWriter.addText(10, 770, 15, model.getMlNo() + " FOR " + model.getMlName() + " " + model.getFrpAddress());
        if (isMap)
            mPDFWriter.addImageKeepRatio(10, 500, 250, 250, MyApplication.bitmapMap);
        else
            mPDFWriter.addImageKeepRatio(10, 500, 250, 250, MyApplication.bitmap);
        mPDFWriter.addImageKeepRatio(10, 70, 250, 250, bitmap);
        mPDFWriter.addText(300, 40, 15, "SIGNATURE OF APPLICANT");
        outputToFile(model.getMlNo() + ".pdf", mPDFWriter.asString(), "ISO-8859-1");
    }

    private void outputToFile(String fileName, String pdfContent, String encoding) {
        File downloads = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS);
        if (!downloads.exists() && !downloads.mkdirs())
            throw new RuntimeException("Could not create download folder");

        File newFile = new File(downloads, fileName);

        try {
            newFile.createNewFile();
            try {
                FileOutputStream pdfFile = new FileOutputStream(newFile);
                pdfFile.write(pdfContent.getBytes(encoding));
                pdfFile.close();
            } catch (FileNotFoundException e) {
                Log.w("PDF", e);
            }
        } catch (IOException e) {
            Log.w("PDF", e);
        }
    }

    @OnClick({R.id.img_switch, R.id.save})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_switch:
                if (isMap)
                    imgPolyline.setImageBitmap(MyApplication.bitmap);
                else
                    imgPolyline.setImageBitmap(MyApplication.bitmapMap);
                isMap = !isMap;
                break;
            case R.id.save:

                new CreateFile().execute();
                break;
        }
    }


    private class CreateFile extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void result) {
            MyApplication.stop_progressbar();
            final Uri selectedUri = Uri.parse(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getAbsolutePath());
            callApi(selectedUri.getPath().toString() + "/" + model.getMlNo()+".pdf");
            Snackbar.make(save, "Pdf file generated", Snackbar.LENGTH_INDEFINITE).setAction("Open", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Uri selectedUri = Uri.parse(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getAbsolutePath());
                    System.out.println("path uri" + selectedUri);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(selectedUri, "resource/folder");
                    if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
                        startActivity(intent);
                    }

                }
            }).show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            generateHelloWorldPDF();
            return null;
        }

        @Override
        protected void onPreExecute() {
            MyApplication.initialize_progressbar(ReportActivity.this);

        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void callApi(String filepath) {
        try {
            MyApplication.initialize_progressbar(ReportActivity.this);

            JSONObject object = new JSONObject();
            try {
                object.put("peopleId",MyApplication.getSharedPrefString("peopleId"));
                object.put("ml_no", model.getMlNo());
                object.put("ml_name", model.getMlName());
                object.put("year", model.getYear());
                object.put("frp_name", model.getFrpName());
                object.put("address", model.getFrpAddress());
                object.put("lattitude", model.getFrpLat());
                object.put("longitude", model.getFrpLong());

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < model.getMeasureList().size(); i++) {
                    JSONObject jobj = new JSONObject();
                    jobj.put("from", model.getMeasureList().get(i).getPointFrom());
                    jobj.put("to", model.getMeasureList().get(i).getPointTo());
                    jobj.put("bearing", model.getMeasureList().get(i).getBearing());
                    jobj.put("distance", model.getMeasureList().get(i).getDistance());
                    jobj.put("latlng", model.getMeasureList().get(i).getLatLng());
                    jsonArray.put(jobj);
                }
                object.put("mesurement_data", jsonArray);


                Log.d("json data created", object.toString());

            } catch (Exception e) {
            }


            File file = new File(filepath);
            RequestBody requestBody2 = RequestBody.create(MediaType.parse(getMimeType(filepath)), file);
            MultipartBody.Part fileToUpload2rt = MultipartBody.Part.createFormData("pdf", file.getName(), requestBody2);

            System.out.println("update json " + object.toString());
            RequestBody data = RequestBody.create(MediaType.parse("text/plain"), object.toString().trim());
            ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
            Call<JsonObject> call = getResponse.uploadReport(fileToUpload2rt, data,MyApplication.getSharedPrefString("userId"));
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    MyApplication.stop_progressbar();
                    if (response.isSuccessful()) {
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response.body().toString());
                            Toast.makeText(ReportActivity.this, "report submit successfully", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        //TestFragment.imageStringpath.clear();
                        MyApplication.stop_progressbar();
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            System.out.println("error response " + jsonObject.toString());

                        } catch (Exception e) {

                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    MyApplication.stop_progressbar();
                    //  TestFragment.imageStringpath.clear();
                    Toast.makeText(ReportActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                    System.out.println("fail response.." + t.toString());
                }
            });
        } catch (Exception e) {
            // TestFragment.imageStringpath.clear();
            System.out.println("Exception..." + e.toString());
            MyApplication.stop_progressbar();
        }

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

}
