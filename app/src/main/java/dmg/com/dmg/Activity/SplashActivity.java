package dmg.com.dmg.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.HorizontalScrollView;

import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.R;

public class SplashActivity extends AppCompatActivity {
    HorizontalScrollView horizontalScrollView;
    Bitmap bitmap;
    String[] permissions = new String[]{Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,

    };
    int[] code = new int[]{1, 2, 3, 4, 5};

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        handlePermissions();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // User may have declined earlier, ask Android if we should show him a reason
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            handlePermissions();
        } else {
            afterPermissions();
        }


    }

    private void handlePermissions() {
        for (int i = 0; i < permissions.length; i++) {
            int permissionCheck = 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                permissionCheck = checkSelfPermission(permissions[i]);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{permissions[i]}, code[i]);
                    return;
                }
            }

        }
        afterPermissions();
    }

    private void afterPermissions() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(MyApplication.getSharedPrefString("name")))
                    MyApplication.activityStart(SplashActivity.this, LogInActivity.class, true);
                else
                    MyApplication.activityStart(SplashActivity.this, HomeActivity.class, true);
            }
        }, 2000);
    }
}
