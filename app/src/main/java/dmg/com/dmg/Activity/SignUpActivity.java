package dmg.com.dmg.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Network.ApiService;
import dmg.com.dmg.R;

public class SignUpActivity extends AppCompatActivity {

    @Bind(R.id.edt_name)
    EditText edtName;
    @Bind(R.id.edt_mobile)
    EditText edtMobile;
    @Bind(R.id.edt_email)
    EditText edtEmail;
    @Bind(R.id.edt_cname)
    EditText edtCname;
    @Bind(R.id.edt_oame)
    EditText edtOame;
    @Bind(R.id.edt_password)
    EditText edtPassword;
    @Bind(R.id.edt_cpassword)
    EditText edtCpassword;
    @Bind(R.id.btn_start)
    TextView btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_start)
    public void onClick() {
        if (isValid())
            signUp();
    }

    private void signUp() {
        JSONObject param = new JSONObject();

        try {
//            param.put("deviceId", "12345");
            param.put("deviceId", MyApplication.getDeviceId(SignUpActivity.this));
            param.put("fullName", edtName.getText());
            param.put("email", edtEmail.getText());
            param.put("mobile", Long.parseLong(edtMobile.getText().toString()));
            param.put("companyName", edtCname.getText());
            param.put("officeName", edtOame.getText());
            param.put("password", edtPassword.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance().makePostCall(SignUpActivity.this, ApiService.SIGN_UP, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.activityStart(SignUpActivity.this, LogInActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(edtName.getText().toString())) {
            MyApplication.setError(edtName, null);
            return false;
        }
        if (TextUtils.isEmpty(edtOame.getText().toString())) {
            MyApplication.setError(edtOame, null);
            return false;
        }
        if (TextUtils.isEmpty(edtCname.getText().toString())) {
            MyApplication.setError(edtCname, null);
            return false;
        }
        if (TextUtils.isEmpty(edtCpassword.getText().toString())) {
            MyApplication.setError(edtCpassword, null);
            return false;
        }
        if (TextUtils.isEmpty(edtPassword.getText().toString())) {
            MyApplication.setError(edtPassword, null);
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            MyApplication.setError(edtName, null);
            return false;
        }
        if (TextUtils.isEmpty(edtMobile.getText().toString())) {
            MyApplication.setError(edtName, null);
            return false;
        }

        if (edtMobile.length() != 10) {
            MyApplication.setError(edtMobile, "Invalid mobile no.");
            return false;
        }
        if (!MyApplication.isEmailValid(edtEmail.getText().toString())) {
            MyApplication.setError(edtEmail, "Invalid Email address");
            return false;
        }

        if (!edtPassword.getText().toString().equals(edtCpassword.getText().toString())) {
            MyApplication.setError(edtCpassword, "Password does not match");
            return false;
        }

        return true;

    }


}
