package dmg.com.dmg.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmg.com.dmg.Adapter.MeasureAdapter;
import dmg.com.dmg.Application.MyApplication;
import dmg.com.dmg.Model.MeasureDataModel;
import dmg.com.dmg.Network.ApiService;
import dmg.com.dmg.R;

public class MeasureByDataActivity extends AppCompatActivity implements MeasureAdapter.EditItem {

    @Bind(R.id.edt_ma_no)
    EditText edtMaNo;
    @Bind(R.id.edt_ml_name)
    EditText edtMlName;
    @Bind(R.id.edt_year)
    EditText edtYear;
    @Bind(R.id.edt_frp_name)
    EditText edtFrpName;
    @Bind(R.id.edt_frp_address)
    EditText edtFrpAddress;

    @Bind(R.id.edt_lat)
    EditText edtLat;
    @Bind(R.id.edt_long)
    EditText edtLong;
    @Bind(R.id.textView4)
    TextView textView4;
    @Bind(R.id.rec_measure)
    RecyclerView recMeasure;
    @Bind(R.id.edt_from)
    EditText edtFrom;
    @Bind(R.id.edt_to)
    EditText edtTo;
    @Bind(R.id.edt_bearing)
    EditText edtBearing;
    @Bind(R.id.edt_distance)
    EditText edtDistance;

    ArrayList<MeasureDataModel.MeasureModel> measureModels = new ArrayList<>();
    MeasureDataModel measureDataModel = new MeasureDataModel();
    MeasureAdapter adapter;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.mine_detail)
    ScrollView mineDetail;
    @Bind(R.id.txt_submit)
    TextView txtSubmit;
    boolean isEdit = false;
    @Bind(R.id.txt_add)
    TextView txtAdd;
    Integer position = null;
    @Bind(R.id.spn_type)
    Spinner spnType;
    @Bind(R.id.bottom)
    LinearLayout bottom;
    @Bind(R.id.txt_no)
    TextView txtNo;
    @Bind(R.id.txt_name)
    TextView txtName;
    @Bind(R.id.txt_remove)
    TextView txtRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure_by_data);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle("Measure By Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        checkVerify();

        spnType.setAdapter(new ArrayAdapter<String>
                (this, R.layout.spinner_item,
                        getResources().getStringArray(R.array.type)));

        spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    txtNo.setText("ML NO");
                    txtName.setText("ML NAME");
                } else if (position == 1) {
                    txtNo.setText("QL NO");
                    txtName.setText("QL NAME");
                } else if (position == 2) {
                    txtNo.setText("STP NO");
                    txtName.setText("STP NAME");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        adapter = new MeasureAdapter(getList(), MeasureByDataActivity.this, this);
        recMeasure.setLayoutManager(new GridLayoutManager(MeasureByDataActivity.this, 1, GridLayoutManager.VERTICAL, false));
        recMeasure.setAdapter(adapter);


        setListner();

    }

    private void setListner() {
        edtLat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    String newString = s.toString();
                    String c = s.charAt(s.length() - 1) + "";
                    if (s.length() > 1 && !s.toString().startsWith(" ")) {


                        if (c.equalsIgnoreCase(" ")) {
                            if (!newString.contains("°")) {
                                newString = newString.replace(" ", "°");
                            } else if (!newString.contains("'")) {
                                newString = newString.replace(" ", "'");
                            } else if (!newString.contains("\"")) {
                                newString = newString.replace(" ", "\"");
                            } else {
                                newString = newString.replace(" ", "");
                            }
                            edtLat.setText(newString);
                            edtLat.setSelection(newString.length());
                        }
                    } else if (s.toString().startsWith(" ")) {
                        newString = newString.replace(" ", "");
                        edtLat.setText(newString);
                        edtLat.setSelection(newString.length());
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edtLong.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    String newString = s.toString();
                    String c = s.charAt(s.length() - 1) + "";
                    if (s.length() > 1 && !s.toString().startsWith(" ")) {


                        if (c.equalsIgnoreCase(" ")) {
                            if (!newString.contains("°")) {
                                newString = newString.replace(" ", "°");
                            } else if (!newString.contains("'")) {
                                newString = newString.replace(" ", "'");
                            } else if (!newString.contains("\"")) {
                                newString = newString.replace(" ", "\"");
                            } else {
                                newString = newString.replace(" ", "");
                            }
                            edtLong.setText(newString);
                            edtLong.setSelection(newString.length());
                        }
                    } else if (s.toString().startsWith(" ")) {
                        newString = newString.replace(" ", "");
                        edtLong.setText(newString);
                        edtLong.setSelection(newString.length());
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtBearing.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    String newString = s.toString();
                    String c = s.charAt(s.length() - 1) + "";
                    if (s.length() > 1 && !s.toString().startsWith(" ")) {


                        if (c.equalsIgnoreCase(" ")) {
                            if (!newString.contains("°")) {
                                newString = newString.replace(" ", "°");
                            } else if (!newString.contains("'")) {
                                newString = newString.replace(" ", "'");
                            } else if (!newString.contains("\"")) {
                                newString = newString.replace(" ", "\"");
                            } else {
                                newString = newString.replace(" ", "");
                            }
                            edtBearing.setText(newString);
                            edtBearing.setSelection(newString.length());
                        }
                    } else if (s.toString().startsWith(" ")) {
                        newString = newString.replace(" ", "");
                        edtBearing.setText(newString);
                        edtBearing.setSelection(newString.length());
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private ArrayList<MeasureDataModel.MeasureModel> getList() {
//        measureModels.add(new MeasureDataModel.MeasureModel("FRP", "A", "346°6'0\"", "3879"));

        /*measureModels.add(new MeasureDataModel.MeasureModel("FRP", "A", "113°0'0\"", "135"));
        measureModels.add(new MeasureDataModel.MeasureModel("A", "B", "108°0'0\"", "60"));
        measureModels.add(new MeasureDataModel.MeasureModel("B", "C", "198°0'0\"", "70"));
        measureModels.add(new MeasureDataModel.MeasureModel("C", "D", "288°0'0\"", "60"));
        measureModels.add(new MeasureDataModel.MeasureModel("D", "A", "18°0'0\"", "70"));*/

       /* measureModels.add(new MeasureDataModel.MeasureModel("FRP", "X", "308°0'0\"", "160"));
        measureModels.add(new MeasureDataModel.MeasureModel("X", "Y", "218°0'0\"", "30"));
        measureModels.add(new MeasureDataModel.MeasureModel("Y", "Z", "290°0'0\"", "24"));
        measureModels.add(new MeasureDataModel.MeasureModel("Z", "A", "200°0'0\"", "10"));
        measureModels.add(new MeasureDataModel.MeasureModel("A", "B", "200°0'0\"", "60"));


        measureModels.add(new MeasureDataModel.MeasureModel("B", "C", "245°0'0\"", "56.56"));
        measureModels.add(new MeasureDataModel.MeasureModel("C", "D", "273°30'0\"", "75.40"));
        measureModels.add(new MeasureDataModel.MeasureModel("D", "E", "20°0'0\"", "82.60"));
        measureModels.add(new MeasureDataModel.MeasureModel("E", "F", "72°0'0\"", "64"));
        measureModels.add(new MeasureDataModel.MeasureModel("F", "A", "110°0'0\"", "60"));*/
        return measureModels;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_data, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            if (addMine())
                MyApplication.showSaveDialog(MeasureByDataActivity.this);
        } else if (id == R.id.open) {
            MyApplication.showOpenDialog(MeasureByDataActivity.this, new MyApplication.OpenFile() {
                @Override
                public void onOpenFile(MeasureDataModel model) {
                    openModel(model);
                }
            });
        } else if (id == android.R.id.home) {
            MyApplication.activityFinish(MeasureByDataActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void addToList() {
        if (TextUtils.isEmpty(edtFrom.getText().toString())) {
            MyApplication.setError(edtFrom, null);
            return;
        }
        if (TextUtils.isEmpty(edtTo.getText().toString())) {
            MyApplication.setError(edtTo, null);
            return;
        }
        if (TextUtils.isEmpty(edtBearing.getText().toString())) {
            MyApplication.setError(edtBearing, null);
            return;
        }

        if (!MyApplication.isDms(edtBearing.getText().toString())) {
            MyApplication.setError(edtBearing, "Enter valid bearing");
            return;
        }


        if (TextUtils.isEmpty(edtDistance.getText().toString())) {
            MyApplication.setError(edtDistance, null);
            return;
        }
        try {
            Float.parseFloat(edtDistance.getText().toString());
        } catch (NumberFormatException e) {
            MyApplication.setError(edtDistance, "Enter valid distance");
            return;
        }

        try {
            MyApplication.dmsToDouble(edtBearing.getText().toString());
        } catch (NumberFormatException e) {
            MyApplication.showToast(MeasureByDataActivity.this, "Enter bearing in valid format");
            return;
        }

        String from = edtFrom.getText().toString();
        String to = edtTo.getText().toString();
        String bearing = edtBearing.getText().toString();
        String distance = edtDistance.getText().toString();

        MeasureDataModel.MeasureModel model = new MeasureDataModel.MeasureModel(from, to, bearing, distance);
        if (position != null) {
            adapter.editItem(position, model);
            isEdit = false;
            txtAdd.setText("ADD");
            txtRemove.setVisibility(View.GONE);
            position = null;
        } else {
            adapter.addItem(model);
        }
        edtFrom.getText().clear();
        edtTo.getText().clear();
        edtBearing.getText().clear();
        edtDistance.getText().clear();
    }

    @OnClick({R.id.txt_add, R.id.txt_remove, R.id.txt_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_add:
                addToList();
                break;
            case R.id.txt_submit:
                if (addMine())

                    startActivity(new Intent(MeasureByDataActivity.this, MapsActivity.class));
                break;
            case R.id.txt_remove:
                adapter.removeItem(position);
                txtRemove.setVisibility(View.GONE);
                isEdit = false;
                txtAdd.setText("ADD");
                position = null;
                edtFrom.getText().clear();
                edtTo.getText().clear();
                edtBearing.getText().clear();
                edtDistance.getText().clear();
                break;
        }
    }

    private boolean addMine() {
        if (TextUtils.isEmpty(edtMaNo.getText().toString())) {
            MyApplication.setError(edtMaNo, null);
            return false;
        }
        if (TextUtils.isEmpty(edtMlName.getText().toString())) {
            MyApplication.setError(edtMlName, null);
            return false;
        }
        if (TextUtils.isEmpty(edtYear.getText().toString())) {
            MyApplication.setError(edtYear, null);
            return false;
        } else if (edtYear.getText().length() != 4) {
            MyApplication.setError(edtYear, "Enter valid year");
            return false;
        }

        if (TextUtils.isEmpty(edtFrpName.getText().toString())) {
            MyApplication.setError(edtFrpName, null);
            return false;
        }
        if (TextUtils.isEmpty(edtFrpAddress.getText().toString())) {
            MyApplication.setError(edtFrpAddress, null);
            return false;
        }
        if (TextUtils.isEmpty(edtLat.getText().toString())) {
            MyApplication.setError(edtLat, null);
            return false;
        }


        if (TextUtils.isEmpty(edtLong.getText().toString())) {
            MyApplication.setError(edtLong, null);
            return false;
        }

       /* if (!MyApplication.isDms(edtLat.getText().toString())) {
            MyApplication.setError(edtLat, "Enter valid Latitude");
            return;
        }
        if (!MyApplication.isDms(edtLong.getText().toString())) {
            MyApplication.setError(edtLong, "Enter valid Longitude");
            return;
        }*/
        try {
            Integer.parseInt(edtYear.getText().toString());
        } catch (NumberFormatException e) {
            MyApplication.setError(edtYear, "Enter valid Year");
            return false;
        }
        try {
            MyApplication.dmsToDouble(edtLat.getText().toString());
            MyApplication.dmsToDouble(edtLong.getText().toString());
        } catch (NumberFormatException e) {
            MyApplication.showToast(MeasureByDataActivity.this, "Enter Latitude and Longitude in valid DMS format");
            return false;
        }


        if (adapter.getList().size() < 1) {
            MyApplication.showToast(MeasureByDataActivity.this, "Enter Points");
            return false;
        }
        String ml_no = edtMaNo.getText().toString();
        String ma_name = edtMlName.getText().toString();
        String year = edtYear.getText().toString();
        String frp_name = edtFrpName.getText().toString();
        String address = edtFrpAddress.getText().toString();
        String lat = edtLat.getText().toString();
        String lng = edtLong.getText().toString();


        MeasureDataModel model = new MeasureDataModel(spnType.getSelectedItem() + "", ml_no, ma_name, year, frp_name, lat, lng, address, adapter.getList());
        MyApplication.model = model;
        return true;
    }


    private void openModel(MeasureDataModel model) {
        edtMaNo.setText(model.getMlNo());
        edtMlName.setText(model.getMlName());
        edtYear.setText(model.getYear());
        edtFrpName.setText(model.getFrpName());
        edtFrpAddress.setText(model.getFrpAddress());
        edtLat.setText(model.getFrpLat());
        edtLong.setText(model.getFrpLong());
        adapter.setData(model.getMeasureList());
    }

    @Override
    public void onEdit(int pos, MeasureDataModel.MeasureModel model) {
        isEdit = true;
        txtAdd.setText("UPDATE");
        position = pos;

        edtFrom.setText(model.getPointFrom());
        edtTo.setText(model.getPointTo());
        edtBearing.setText(model.getBearing());
        edtDistance.setText(model.getDistance());

        txtRemove.setVisibility(View.VISIBLE);
    }

    private void checkVerify() {
        ApiService.getInstance().makeGetCall(MeasureByDataActivity.this, ApiService.VERIFY_USER+"?peopleId="+MyApplication.getSharedPrefString("peopleId"), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                try{
                    Log.d("response",response+"");
                    if (response.getJSONObject("success").getJSONObject("data").getBoolean("status")==false){
                        MyApplication.setSharedPrefString("name", "");
                        MyApplication.setSharedPrefString("userId", "");
                        MyApplication.activityStart(MeasureByDataActivity.this, LogInActivity.class, true);
                    }
                }catch (Exception e){

                }


            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
