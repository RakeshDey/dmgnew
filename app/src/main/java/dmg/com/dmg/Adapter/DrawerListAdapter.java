package dmg.com.dmg.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dmg.com.dmg.R;


/**
 * Created by ramees on 8/16/2016.
 */
public class DrawerListAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Activity activity;
    ArrayList<DrawerItem> list = new ArrayList<>();
    private int selectedPosition = 0;

    public DrawerListAdapter(Activity activity) {
// TODO Auto-generated constructor stub
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public void addMenu(String menu) {
        list.add(new DrawerItem(menu));
        notifyDataSetChanged();
    }

    public String getMenu(int position) {
        return list.get(position).getMenu();
    }

    @Override
    public int getCount() {
// TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
// TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
// TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        Holder holder = new Holder();
        View view;
        view = inflater.inflate(R.layout.layout_drawer_item, null);
        holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
        holder.tv_title.setText(list.get(position).getMenu());

        if (position == selectedPosition) {
            view.setBackgroundColor(Color.parseColor("#111A2B"));
            holder.tv_title.setTextColor(Color.WHITE);

        } else {
            view.setBackgroundColor(Color.TRANSPARENT);

        }
        return view;
    }

    public class Holder {
        TextView tv_title;
        ImageView im_icon;
    }

    private class DrawerItem {
        private String menu;

        public DrawerItem(String menu) {
            this.menu = menu;
        }

        public String getMenu() {
            return menu;
        }

    }
}