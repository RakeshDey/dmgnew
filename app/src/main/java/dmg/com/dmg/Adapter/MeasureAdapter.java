package dmg.com.dmg.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dmg.com.dmg.Model.MeasureDataModel;
import dmg.com.dmg.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class MeasureAdapter extends RecyclerView.Adapter<MeasureAdapter.ViewHolder> {
    int selectedPosition = 0;
    EditItem callback;
    private ArrayList<MeasureDataModel.MeasureModel> list;
    private int rowLayout;
    private Activity activity;
    private boolean isOpen = false;

    public MeasureAdapter(ArrayList<MeasureDataModel.MeasureModel> list, Activity activity, EditItem callback) {
        this.list = list;
        this.rowLayout = R.layout.item_measure;
        this.activity = activity;
        this.callback = callback;


    }

    public void editItem(int pos, MeasureDataModel.MeasureModel model) {
        list.get(pos).setPointFrom(model.getPointFrom());
        list.get(pos).setPointTo(model.getPointTo());
        list.get(pos).setBearing(model.getBearing());
        list.get(pos).setDistance(model.getDistance());

        notifyDataSetChanged();

    }

    public void removeItem(int pos) {
        list.remove(pos);
        notifyItemRemoved(pos);
    }

    public ArrayList<MeasureDataModel.MeasureModel> getList() {
        return list;
    }

    public void setData(ArrayList<MeasureDataModel.MeasureModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public void clearData() {
        if (list != null)
            list.clear();
    }


    @Override
    public MeasureAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new MeasureAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MeasureAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_from.setText(list.get(position).getPointFrom());
        viewHolder.txt_to.setText(list.get(position).getPointTo());
        viewHolder.txt_bearing.setText(list.get(position).getBearing() + "");
        viewHolder.txt_distance.setText(list.get(position).getDistance() + "");

        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                callback.onEdit(position, list.get(position));
                return false;
            }
        });
    }


    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public void addItem(MeasureDataModel.MeasureModel model) {
        list.add(model);
        notifyItemInserted(list.size());

    }

    public interface EditItem {
        void onEdit(int pos, MeasureDataModel.MeasureModel model);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_from, txt_to, txt_bearing, txt_distance;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_from = itemView.findViewById(R.id.txt_from);
            txt_to = itemView.findViewById(R.id.txt_to);
            txt_bearing = itemView.findViewById(R.id.txt_bearing);
            txt_distance = itemView.findViewById(R.id.txt_distance);
        }
    }
}
