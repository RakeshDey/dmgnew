package dmg.com.dmg.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import dmg.com.dmg.Model.MesurementBinder;
import dmg.com.dmg.Network.ApiService;
import dmg.com.dmg.R;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class MesurementAdapter extends RecyclerView.Adapter<MesurementAdapter.MyViewHolder> {
    private List<MesurementBinder> feedsListall;
    private List<MesurementBinder> mArrayList;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    int position_select = 0;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;

    public MesurementAdapter(Context context, List<MesurementBinder> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        this.mArrayList = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
    }

    public void setList(List<MesurementBinder> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.mesurement_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MesurementBinder feeds = feedsListall.get(position);
        //Pass the values of feeds object to Views
        holder.ml_no.setText(feeds.getMl_no().trim());
        holder.ml_name.setText(feeds.getMl_name());
        holder.year.setText(feeds.getYear());
        holder.frp_name.setText(feeds.getFrp_name());
        holder.address.setText(feeds.getAddress());

        holder.pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApiService.BASE_URL2+feeds.getPdf()));
                context.startActivity(browserIntent);
            }
        });


    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return feedsListall.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView ml_no, ml_name, year,frp_name,address;
        private Button pdf;

        public MyViewHolder(View itemView) {
            super(itemView);
            ml_no = (TextView) itemView.findViewById(R.id.txt_ma_no);
            ml_name = (TextView) itemView.findViewById(R.id.txt_ml_name);
            year = (TextView) itemView.findViewById(R.id.txt_year);
            frp_name = (TextView) itemView.findViewById(R.id.txt_frpname);
            address = (TextView) itemView.findViewById(R.id.txt_address);
            pdf=(Button)itemView.findViewById(R.id.pdf);

        }
    }

}
