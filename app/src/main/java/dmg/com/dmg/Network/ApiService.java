package dmg.com.dmg.Network;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import dmg.com.dmg.Application.MyApplication;


/**
 * Created by Advosoft2 on 1/22/2018.
 */

public class ApiService {

    public static String BASE_URL = "http://139.59.71.150:3005/api/";
    public static String BASE_URL2 = "http://139.59.71.150:3005";
    public static String LOG_IN = BASE_URL + "People/login";
    public static String FORGOT = BASE_URL + "People/reset";
    public static String LOG_OUT = BASE_URL + "People/logout";
    public static String SIGN_UP = BASE_URL + "People/signup";
    public static String VERIFY_USER = BASE_URL + "People/verifyUser";
    public static String GET_List = "Measures/getMyMeasures";
    private static ApiService apiService;
    ProgressDialog dialog;
    private Activity activity;

    private ApiService(Activity activity) {
        this.activity = activity;
    }

    private ApiService() {

    }

    public static synchronized ApiService getInstance(Activity act) {
        if (apiService == null) {
            apiService = new ApiService(act);
        }
        return apiService;
    }

    public static synchronized ApiService getInstance() {
        if (apiService == null) {
            apiService = new ApiService();
        }
        return apiService;
    }


    public void makePostCall(final Context context, String url, final JSONObject params, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                MyApplication.stop_progressbar();
                responseCallback.onResponseSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                if (error.networkResponse != null) {
                    try {
                        JSONObject errorObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                        MyApplication.showToast(context, errorObject.optJSONObject("error").optString("message"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseCallback.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                if (MyApplication.getSharedPrefString("userId").compareTo("")!=0){
                    params.put("access_token",MyApplication.getSharedPrefString("userId"));
                }

                return params;
            }

            @Override
            public byte[] getBody() {
                return params.toString().getBytes();
            }

        };
        MyApplication.getInstance().addToRequestQueue(request);
    }

    public void makePostCallAuth(final Context context, String url, final JSONObject params, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                MyApplication.stop_progressbar();
                responseCallback.onResponseSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                if (error.networkResponse != null) {
                    try {
                        String errorMsg = new String(error.networkResponse.data, "UTF-8");
                        MyApplication.showToast(context, errorMsg);
                        Log.e("Error Volley", errorMsg);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                responseCallback.onError(error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
//                params.put("Authorization", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
                return params;
            }

            @Override
            public byte[] getBody() {
                if (params != null)
                    return params.toString().getBytes();
                else
                    return null;
            }

        };
        MyApplication.getInstance().addToRequestQueue(request);
    }

    public void makeGetCallArray(Context context, String url, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                JSONObject object = new JSONObject();
                try {
                    object.put("array", response);
                    MyApplication.stop_progressbar();
                    responseCallback.onResponseSuccess(object);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                responseCallback.onError(error);
            }
        });
        MyApplication.getInstance().addToRequestQueue(request);

    }

    public void multipartCall(final Context context, String url, final Map<String, DataPart> params, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    MyApplication.stop_progressbar();
                    responseCallback.onResponseSuccess(new JSONObject(new String(response.data)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                try {
                    String errorMsg = new String(error.networkResponse.data, "UTF-8");
                    MyApplication.showToast(context, errorMsg);
                    Log.e("Error Volley", errorMsg);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                responseCallback.onError(error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Authorization", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    public void makeGetCall(Context context, String url, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.stop_progressbar();
                        responseCallback.onResponseSuccess(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                responseCallback.onError(error);
            }

        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header_param = new HashMap<>();
                header_param.put("Authorization", MyApplication.getSharedPrefString("userId"));
                return header_param;
            }
        };
        MyApplication.getInstance().addToRequestQueue(getRequest);
    }


    public interface OnResponse {
        void onResponseSuccess(JSONObject response);

        void onError(VolleyError volleyError);

    }
}
