package dmg.com.dmg.Network;


import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiConfig {

    @Multipart
    @POST("/api/Measures/addMeasurements")
    Call<JsonObject> uploadReport(@Part MultipartBody.Part files,
                                  @Part("data") RequestBody lookingFor,
                                  @Query("access_token") String accessToken

    );


}
