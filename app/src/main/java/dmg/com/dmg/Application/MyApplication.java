package dmg.com.dmg.Application;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dmg.com.dmg.Model.MeasureDataModel;
import dmg.com.dmg.R;


/**
 * Created by Advosoft2 on 1/22/2018.
 */

public class MyApplication extends Application {
    public static String SHARED_PREF_NAME = "DMG_PREF";
    public static Gson gson = new Gson();
    public static MeasureDataModel model = new MeasureDataModel();
    public static Bitmap bitmap = null;
    public static Bitmap bitmapMap = null;
    public static String APP_DIR = Environment.getExternalStorageDirectory() + "/DMG/";
    static Dialog mDialog = null;
    private static MyApplication application;
    private static ProgressDialog dialog;
    public List<String> fileList = new ArrayList<>();
    private RequestQueue mRequestQueue;
    private Context context;

    public static synchronized MyApplication getInstance() {
        return application;
    }

    public static void showProgressDialog(String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.setMessage(message);
        } else
            dialog = ProgressDialog.show(MyApplication.getInstance().getApplicationContext(), "", message, true);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    public static void hideProgressDialog(Activity activity) {
        if (dialog != null && activity != null) {
            if (dialog.isShowing()) {
                try {
                    dialog.dismiss();
                } catch (IllegalStateException e) {
                }
            }
        }
    }

    public static byte[] getBytesFromBitmap(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void activityStart(Activity activity, Class<?> aClass, boolean finish) {
        activity.startActivity(new Intent(activity, aClass));
        activity.overridePendingTransition(R.anim.exit, R.anim.enter);
        if (finish)
            activity.finish();
    }

    public static void activityStart(Activity activity, Class<?> aClass, boolean finish, Bundle bundle) {
        activity.startActivity(new Intent(activity, aClass).putExtra("bundle", bundle));
        activity.overridePendingTransition(R.anim.exit, R.anim.enter);
        if (finish)
            activity.finish();
    }

    public static void activityFinish(Activity activity) {
        activity.finish();
        activity.overridePendingTransition(R.anim.exit, R.anim.enter);
    }

    public static String getCurrentUTC(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            date = format.parse(time);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    public static String getDeviceId(Context ctx) {

        String android_id = "";
        final TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = ""
                + android.provider.Settings.Secure.getString(
                ctx.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(),
                ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        android_id = deviceUuid.toString();

        return tmDevice;

    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void initialize_progressbar(Context context) {
        if (mDialog != null) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }
        if (context != null) {
            mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setContentView(R.layout.dialog_progress);
            mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            mDialog.setCancelable(false);

            LinearLayout linearLayout = (LinearLayout) mDialog.findViewById(R.id.linearLayoutLoader);

            ImageView imageView = new ImageView(context);

            Float width = context.getResources().getDimension(R.dimen._55sdp);

            imageView.setLayoutParams(new LinearLayout.LayoutParams(
                    width.intValue(),
                    width.intValue()
            ));


            imageView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
            Glide.with(context)
                    .load(R.drawable.loader_animation)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageViewTarget);


            linearLayout.addView(imageView);

            try {
                mDialog.show();
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
        }
    }

    public static String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String alternative = "alternatives=true";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + alternative;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        Log.e("url", url);

        return url;
    }

    public static void stop_progressbar() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public static String utcToDate(String date) {
        try {
            // 2017-03-29 14:40:26
            //  String strCurrentDate = "Wed, 18 Apr 2012 07:55:29 +0000";
            //  String strCurrentDate = "2018-01-16T12:53:19.200Z";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            //SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss Z");
            Date newDate = null;

            newDate = format.parse(date);
            //  format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
//            format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            format = new SimpleDateFormat("dd-MM-yyyy");
            return format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static String utcToTime(String date) {
        try {
            // 2017-03-29 14:40:26
            //  String strCurrentDate = "Wed, 18 Apr 2012 07:55:29 +0000";
            //  String strCurrentDate = "2018-01-16T12:53:19.200Z";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            //SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss Z");
            Date newDate = null;

            newDate = format.parse(date);
            //  format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
//            format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            format = new SimpleDateFormat("hh:mm");
            return format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static long getSharedPrefLong(String preffConstant) {
        long longValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        longValue = sp.getLong(preffConstant, 0);
        return longValue;
    }

    public static void setSharedPrefLong(String preffConstant, long longValue) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(preffConstant, longValue);
        editor.commit();
    }

    public static String getSharedPrefString(String preffConstant) {
        String stringValue = "";
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        stringValue = sp.getString(preffConstant, "");
        return stringValue;
    }

    public static void setSharedPrefString(String preffConstant,
                                           String stringValue) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(preffConstant, stringValue);
        editor.commit();
    }

    public static int getSharedPrefInteger(String preffConstant) {
        int intValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        intValue = sp.getInt(preffConstant, 0);
        return intValue;
    }

    public static void printSharedPref() {
        int intValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Log.e("SP", sp.toString());

    }

    public static void setSharedPrefInteger(String preffConstant, int value) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(preffConstant, value);
        editor.commit();
    }

    public static float getSharedPrefFloat(String preffConstant) {
        float floatValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                preffConstant, 0);
        floatValue = sp.getFloat(preffConstant, 40);
        return floatValue;
    }

    public static void setSharedPrefFloat(String preffConstant, float floatValue) {
        SharedPreferences sp = application.getSharedPreferences(
                preffConstant, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(preffConstant, floatValue);
        editor.commit();
    }

    public static void setSharedPrefArray(String preffConstant, float floatValue) {
        SharedPreferences sp = application.getSharedPreferences(
                preffConstant, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(preffConstant, floatValue);
        editor.commit();
    }

    public static boolean getStatus(String name) {
        boolean status;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        status = sp.getBoolean(name, false);
        return status;
    }

    public static void setStatus(String name, boolean istrue) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(name, istrue);
        editor.commit();
    }

    public static void setDouble(String name, Double value) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(name, value.toString());
        editor.commit();
    }

    public static Double getDouble(String name) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Double value = Double.parseDouble(sp.getString(name, ""));

        return value;
    }

    public static Bitmap getBitmapFromView(View def) {
        Bitmap bitmap;
        def.setDrawingCacheEnabled(true);
        def.buildDrawingCache(true);

        bitmap = Bitmap.createBitmap(def.getWidth(), def.getHeight(),
                Bitmap.Config.ARGB_8888);
        def.layout(0, 0, def.getLayoutParams().width,
                def.getLayoutParams().height);
        Canvas c = new Canvas(bitmap);
        def.draw(c);

        def.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public static boolean isDms(String dms) {
        Pattern pattern = Pattern.compile("(?:(\\d+)°)?(?:(\\d+)')?(?:(\\d+)\")?");
        Matcher m = pattern.matcher(dms);
//        return m.matches();
        return true;
    }

    public static Double dmsToDouble(String dms) throws NumberFormatException {
        double result = 0;
        if (!dms.isEmpty()) {
          /*  Pattern pattern = Pattern.compile("(?:(\\d+)°)?(?:(\\d+)')?(?:(\\d+)\")?");
            Matcher m = pattern.matcher(dms);
            if (m.matches()) {*/
            dms = dms.replace("°", "B");
            dms = dms.replace("'", "B");
            dms = dms.replace("\"", "B");
            String[] degreeMinute = dms.split("B");
            double deegree = 0.0d, minute = 0.0d, sec = 0.0d;
            if (degreeMinute.length > 0) {
                deegree = Double.parseDouble(degreeMinute[0]);
            }
            if (degreeMinute.length > 1) {
                minute = Double.parseDouble(degreeMinute[1]);
            }
            if (degreeMinute.length > 2) {
                sec = Double.parseDouble(degreeMinute[2]);
            }
            result = deegree + (minute / 60) + (sec / 3600);
            return result;
//            }
        }
        return null;
    }

    public static String doubleToDms(Double value) {
        double d = Math.floor(value);
        double minfloat = (value - d) * 60.0d;
        double m = Math.floor(minfloat);
        double secfloat = (minfloat - m) * 60.0d;
        double s = BigDecimal.valueOf(secfloat).setScale(6, RoundingMode.HALF_UP).doubleValue();
        if (s == 60.0d) {
            m++;
            s = 0.0d;
        }
        if (m == 60.0d) {
            d++;
            m = 0.0d;
        }
        String dms = d + "°" + m + "'" + s + "\"";
        return dms;
    }

    public static String doubleToDmslat(Double value) {
        double d = Math.floor(value);
        double minfloat = (value - d) * 60.0d;
        double m = Math.floor(minfloat);
        double secfloat = (minfloat - m) * 60.0d;
        double s = BigDecimal.valueOf(secfloat).setScale(6, RoundingMode.HALF_UP).doubleValue();
        String direction;
        if (value > 0){
            direction="N";
        }else {
            direction="S";
        }
        if (s == 60.0d) {
            m++;
            s = 0.0d;
        }
        if (m == 60.0d) {
            d++;
            m = 0.0d;
        }
        String dms = (int)d + " " + (int)m + " " + s + " "+direction;
        return dms;
    }

    public static String doubleToDmsLong(Double value) {
        double d = Math.floor(value);
        double minfloat = (value - d) * 60.0d;
        double m = Math.floor(minfloat);
        double secfloat = (minfloat - m) * 60.0d;
        double s = BigDecimal.valueOf(secfloat).setScale(6, RoundingMode.HALF_UP).doubleValue();
        String direction;
        if (value > 0){
            direction="W";
        }else {
            direction="E";
        }
        if (s == 60.0d) {
            m++;
            s = 0.0d;
        }
        if (m == 60.0d) {
            d++;
            m = 0.0d;
        }
        String dms = (int)d + " " + (int)m + " " + s + " "+direction;
        return dms;
    }

    public static String doubleToDmsNew(Double value) {
        double d = Math.floor(value);
        double minfloat = (value - d) * 60.0d;
        double m = Math.floor(minfloat);
        double secfloat = (minfloat - m) * 60.0d;
        double s = BigDecimal.valueOf(secfloat).setScale(6, RoundingMode.HALF_UP).doubleValue();
        if (s == 60.0d) {
            m++;
            s = 0.0d;
        }
        if (m == 60.0d) {
            d++;
            m = 0.0d;
        }
        String dms = (int)d + "°" + (int)m + "'" + s + "\"";
        return dms;
    }

    public static void setError(EditText editText, String msg) {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.setError("Field can't be remain blank");
            editText.requestFocus();
        } else if (msg != null) {
            editText.setError(msg);
            editText.requestFocus();
        }
    }

    public static void checkError(EditText editText, String msg) {
        editText.setError(msg == null ? "Field can't be remain blank" : msg);
        editText.requestFocus();
    }

    public static void saveModel(MeasureDataModel model, String name) {
        try {
            File pathFile = new File(APP_DIR);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }
            File f = new File(APP_DIR + name + ".ser");
            if (f.exists()) {
                f.delete();
                System.out.println("old file deleted>>>>>>>>> ");
            }
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(model);
            oos.close();
            getInstance().fileList = new ArrayList<>();
            getInstance().loadFiles();
            System.out.println("my file replaced>>>>>>>>> ");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
//    @SuppressLint("SdCardPath")
    public static MeasureDataModel getModel(String name) {
        File f = new File(APP_DIR + name);
        MeasureDataModel model = new MeasureDataModel();
        if (f.exists()) {
            try {
                System.gc();
                FileInputStream fis = new FileInputStream(f);
                ObjectInputStream is = new ObjectInputStream(fis);
                Object readObject = is.readObject();
                is.close();
                if (readObject != null && readObject instanceof MeasureDataModel) {
                    return (MeasureDataModel) readObject;
                }


            } catch (StreamCorruptedException e) {
                e.printStackTrace();
            } catch (OptionalDataException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static int getDisplayWidth() {
        WindowManager wm = (WindowManager)
                getInstance().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public static int getDisplayHeight() {
        WindowManager wm = (WindowManager)
                getInstance().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        return height;
    }

    public static void showSaveDialog(final Activity activity) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        final View yourCustomView = inflater.inflate(R.layout.dialog_otp, null);
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(yourCustomView)
                .create();
        final EditText edtFileName = yourCustomView.findViewById(R.id.edt_file);

        final TextView txtSubmit = yourCustomView.findViewById(R.id.txt_submit);
        final TextView txtResend = yourCustomView.findViewById(R.id.txt_resend);
        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtFileName.getText().toString())) {
                    MyApplication.showToast(activity, "Enter File Name");
                } else if (MyApplication.model == null) {
                    MyApplication.showToast(activity, "Enter complete data");
                    dialog.dismiss();
                } else {
                    MyApplication.saveModel(MyApplication.model, edtFileName.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showOpenDialog(final Activity activity, final OpenFile callback) {
        if (getInstance().fileList != null && getInstance().fileList.size() < 1) {
            showToast(getInstance(), "No files in directory");
            return;
        }

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View yourCustomView = inflater.inflate(R.layout.dialog_open, null);
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(yourCustomView)
                .create();
        final ListView listFile = yourCustomView.findViewById(R.id.list_file);
        final ArrayAdapter adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1,
                MyApplication.getInstance().fileList);
        listFile.setAdapter(adapter);
        listFile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                callback.onOpenFile(MyApplication.getModel(MyApplication.getInstance().fileList.get(position)));
                dialog.dismiss();
            }
        });

        listFile.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final PopupMenu popup = new PopupMenu(activity, view);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        File file = new File(APP_DIR + getInstance().fileList.get(position));
                        file.delete();
                        getInstance().loadFiles();

                        popup.dismiss();

                        dialog.dismiss();
                        return true;
                    }
                });
                popup.inflate(R.menu.menu_delete);
                popup.show();
                return true;
            }
        });
        dialog.show();
    }

    public void loadFiles() {
        fileList = new ArrayList<>();
        File directory = new File(APP_DIR);
        File[] files = directory.listFiles();
        if (files != null && files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                fileList.add(files[i].getName());
            }
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        context = getApplicationContext();
        dialog = new ProgressDialog(context);
        loadFiles();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });

    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        Intent intent = new Intent();
        intent.setAction("com.mydomain.SEND_LOG_DMG"); // see step 5.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
        startActivity(intent);
        // kill off the crashed app
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void stopService() {
        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        mActivityManager.restartPackage(getPackageName());
    }

    public interface OpenFile {
        void onOpenFile(MeasureDataModel model);
    }
}
