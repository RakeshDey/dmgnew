package dmg.com.dmg.Model;

import android.text.TextUtils;

import dmg.com.dmg.Application.MyApplication;

/**
 * Created by Advosoft2 on 3/23/2018.
 */

public class GraphModel {
    Double fx;
    Double x1;
    Double fy;
    Double y1;

    public GraphModel(Double x1, Double y1, String br, String ds) {
        this.x1 = x1;
        this.y1 = y1;

        if (!TextUtils.isEmpty(br) && !TextUtils.isEmpty(ds)) {
            double bearing = MyApplication.dmsToDouble(br);
            float distance = Float.parseFloat(ds);

            this.fx = ((double) distance) * Math.cos(bearing);
            this.fy = ((double) distance) * Math.sin(bearing);
            this.x1 = fx + this.x1;
            this.y1 = fy + this.y1;
        }
    }

    public Double getFx() {
        return fx;
    }

    public void setFx(double fx) {
        this.fx = fx;
    }

    public Double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public Double getFy() {
        return fy;
    }

    public void setFy(double fy) {
        this.fy = fy;
    }

    public Double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }
}
