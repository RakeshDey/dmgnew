package dmg.com.dmg.Model;

import android.text.TextUtils;

import java.text.DecimalFormat;

/* compiled from: Description */
public class Latlon {
    String DM = "";
    double lat;
    String latlon = "";
    double lon;
    double plat;
    double plon;
    double rlat;
    double rlon;

    public Latlon(double radlat, double radlon, String br, String ds) {
        this.lat = (180.0d * radlat) / 3.141592653589793d;
        this.lon = (180.0d * radlon) / 3.141592653589793d;
        this.rlat = radlat;
        this.rlon = radlon;
        String brr = br;
        String brrr = ds;
        if (!TextUtils.isEmpty(brr) && !TextUtils.isEmpty(brrr)) {
            double z = (double) degrad(brr, 0.0f);
            double dis = Double.parseDouble(brrr);
            double nlt = Math.asin((Math.sin(radlat) * Math.cos(dis / 6371229.0d)) + ((Math.cos(radlat) * Math.sin(dis / 6371229.0d)) * Math.cos(z)));
            double nlg = radlon + Math.atan2((Math.sin(z) * Math.sin(dis / 6371229.0d)) * Math.cos(radlat), Math.cos(dis / 6371229.0d) - (Math.sin(radlat) * Math.sin(nlt)));
            double lat1 = (180.0d * nlt) / 3.141592653589793d;
            double lon1 = (180.0d * nlg) / 3.141592653589793d;
            String stre = dms(lat1, lon1);
            String stre1 = dm(lat1, lon1);
            this.plat = (180.0d * radlat) / 3.141592653589793d;
            this.plon = (180.0d * radlon) / 3.141592653589793d;
            this.rlat = nlt;
            this.rlon = nlg;
            this.lat = lat1;
            this.lon = lon1;
            this.latlon = stre;
            this.DM = stre1;
        }
    }

    public static double degrad(String str, double A) {
        String[] latlon = str.split("[\\s]|[\"*]|[\"#]|[\",]|[\"/]");
        try {
            if (latlon.length == 3) {
                double a = Double.parseDouble(latlon[0]);
                double b = Double.parseDouble(latlon[1]);
                double c = Double.parseDouble(latlon[2]);
                if (c == 0.0d) {
                    return 0.0d;
                }
                return (3.141592653589793d * ((((((b / 60.0d) + a) + (c / 3600.0d)) + A) + 360.0d) % 360.0d)) / 180.0d;
            } else if (latlon.length == 1) {
                return (3.141592653589793d * (((A + Double.parseDouble(latlon[0])) + 360.0d) % 360.0d)) / 180.0d;
            } else {
                if (latlon.length != 2) {
                    return 0.0d;
                }
                return (3.141592653589793d * ((((A + Double.parseDouble(latlon[0])) + (Double.parseDouble(latlon[1]) / 60.0d)) + 360.0d) % 360.0d)) / 180.0d;
            }
        } catch (NumberFormatException e) {
            return 0.0d;
        }
    }

    public String dms(double d, double q) {
        int dr0 = (int) d;
        int mn0 = (int) ((d - ((double) dr0)) * 60.0d);
        double sc00 = Double.parseDouble(new DecimalFormat("00.00").format(60.0d * (((d - ((double) dr0)) * 60.0d) - ((double) mn0))));
        String deg = Integer.toString(dr0);
        String min = Integer.toString(mn0);
        String sec = Double.toString(sc00);
        int dr00 = (int) q;
        int mn00 = (int) ((q - ((double) dr00)) * 60.0d);
        double sc000 = Double.parseDouble(new DecimalFormat("00.00").format(60.0d * (((q - ((double) dr00)) * 60.0d) - ((double) mn00))));
        String deg1 = Integer.toString(dr00);
        String min1 = Integer.toString(mn00);
        return "N" + deg + "°" + min + "'" + " " + sec + "''" + " " + "E" + deg1 + "°" + min1 + "'" + " " + Double.toString(sc000) + "''";
    }

    public String dm(double d, double q) {
        int dr0 = (int) d;
        double sc00 = Double.parseDouble(new DecimalFormat("00.0000").format((d - ((double) dr0)) * 60.0d));
        String deg = Integer.toString(dr0);
        String min = Double.toString(sc00);
        int dr00 = (int) q;
        double sc000 = Double.parseDouble(new DecimalFormat("00.0000").format((q - ((double) dr00)) * 60.0d));
        String deg1 = Integer.toString(dr00);
        return "N" + deg + "°" + " " + min + "'" + " " + "E" + deg1 + "°" + " " + Double.toString(sc000) + "'";
    }
}
