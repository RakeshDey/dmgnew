package dmg.com.dmg.Model;

import java.io.Serializable;

/**
 * Created by Advosoft2 on 4/6/2018.
 */

public class MesurementBinder implements Serializable {
    String ml_no;
    String ml_name;
    String year;
    String frp_name;
    String address;
    String pdf;

    public String getMl_no() {
        return ml_no;
    }

    public void setMl_no(String ml_no) {
        this.ml_no = ml_no;
    }

    public String getMl_name() {
        return ml_name;
    }

    public void setMl_name(String ml_name) {
        this.ml_name = ml_name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFrp_name() {
        return frp_name;
    }

    public void setFrp_name(String frp_name) {
        this.frp_name = frp_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}
