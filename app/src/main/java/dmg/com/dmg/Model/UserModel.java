package dmg.com.dmg.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserModel {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("mobile")
    @Expose
    private Long mobile;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("officeName")
    @Expose
    private String officeName;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("adminVerified")
    @Expose
    private Boolean adminVerified;
    @SerializedName("mobileVerified")
    @Expose
    private Boolean mobileVerified;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getAdminVerified() {
        return adminVerified;
    }

    public void setAdminVerified(Boolean adminVerified) {
        this.adminVerified = adminVerified;
    }

    public Boolean getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(Boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("email", email).append("id", id).append("fullName", fullName).append("mobile", mobile).append("companyName", companyName).append("officeName", officeName).append("deviceId", deviceId).append("createdAt", createdAt).append("adminVerified", adminVerified).append("mobileVerified", mobileVerified).toString();
    }

}