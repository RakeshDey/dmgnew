package dmg.com.dmg.Model;

import android.util.Log;

import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dmg.com.dmg.Application.MyApplication;

/**
 * Created by Advosoft2 on 3/9/2018.
 */
public class MeasureDataModel implements Serializable {
    private static final long serialVersionUID = 11212L;
    String mlNo, mlName, frpName, frpLat, frpLong, frpAddress, year;
    ArrayList<MeasureModel> measureList = new ArrayList<>();
    String type;

    public MeasureDataModel() {
    }

    public String getType() {
        return type;
    }

    public MeasureDataModel(String type, String mlNo, String mlName, String year, String frpName, String frpLat, String frpLong, String frpAddress, ArrayList<MeasureModel> measureList) {
        this.mlNo = mlNo;
        this.mlName = mlName;
        this.frpName = frpName;
        this.frpLat = frpLat;
        this.frpLong = frpLong;
        this.frpAddress = frpAddress;
        this.measureList = measureList;
        this.year = year;
        this.type = type;
        updateList(new LatLng(MyApplication.dmsToDouble(frpLat), MyApplication.dmsToDouble(frpLong)), this.measureList);

    }

    public String getYear() {
        return year;
    }

    public List<com.google.android.gms.maps.model.LatLng> getClosedArea() {
        List<com.google.android.gms.maps.model.LatLng> list = new ArrayList<>();
        boolean canAdd = false;
        String lastPoint = measureList.get(measureList.size() - 1).getPointTo();
        for (int i = 0; i < this.measureList.size(); i++) {
            if (canAdd) {
                list.add(new com.google.android.gms.maps.model.LatLng
                        (measureList.get(i).getLatLng().getLatitude()
                                , measureList.get(i).getLatLng().getLongitude()));

                Log.d("latitude", measureList.get(i).getLatLng().getLatitude()+"");

                continue;
            }
            if (measureList.get(i).getPointTo().equalsIgnoreCase(lastPoint)) {
                canAdd = true;
            }
        }
        list.add(list.get(0));
        return list;
    }

    private void updateList(LatLng frpLatLng, List<MeasureModel> list) {
        LatLng lastPoint = frpLatLng;

        for (int i = 0; i < list.size(); i++) {
            double bearing = MyApplication.dmsToDouble(list.get(i).getBearing());
           /* if (bearing <= 90.0) {

            } else if (bearing > 90.0 && bearing <= 180) {
                bearing = bearing + 90;
            } else if (bearing > 180.0 && bearing <= 270) {
                bearing = bearing - 180;
            } else if (bearing > 270.0 && bearing <= 360) {
                bearing = 360 - bearing;
            }*/
            LatLng simpleLast = new LatLng(lastPoint.getLatitude(), lastPoint.getLongitude());

            LatLng newPoint1 = LatLngTool.travel(simpleLast, bearing, (Double.parseDouble(list.get(i).getDistance())), LengthUnit.METER);
            LatLng newPoint = new LatLng(newPoint1.getLatitude(), newPoint1.getLongitude());

            list.get(i).setLatLng(newPoint);
            lastPoint = newPoint;
            if (i == list.size() - 1) {
                int position = getPosition(list.get(i).getPointTo());
                if (position > -1) {
                    list.get(i).setLatLng(list.get(position).getLatLng());
                }
            }
        }
    }

    private int getPosition(String pointTo) {
        for (int i = 0; i < getMeasureList().size(); i++) {
            String listPoint = getMeasureList().get(i).getPointTo();
            if (pointTo.equalsIgnoreCase(listPoint)) {
                return i;
            }
        }
        return -1;
    }


    public String getMlNo() {
        return mlNo;
    }

    public String getMlName() {
        return mlName;
    }

    public String getFrpName() {
        return frpName;
    }

    public String getFrpLat() {
        return frpLat;
    }

    public String getFrpLong() {
        return frpLong;
    }

    public String getFrpAddress() {
        return frpAddress;
    }

    public ArrayList<MeasureModel> getMeasureList() {
        return measureList;
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < measureList.size(); i++) {
            s = s + measureList.get(i).toString() + "\n";
        }
        return new ToStringBuilder(this).append("mlNo", mlNo).append("mlName", mlName).append("year", year).append("frpName", frpName).append("frpAddress", frpAddress).append("frpLat", frpLat).append("frpLong", frpLong).append("list", s).toString();

    }

    public static class MeasureModel implements Serializable {
        private static final long serialVersionUID = 3454654L;
        String pointFrom, pointTo, bearing, distance;
        LatLng latLng;

        public MeasureModel(String pointFrom, String pointTo, String bearing, String distance) {
            this.pointFrom = pointFrom;
            this.pointTo = pointTo;
            this.bearing = bearing;
            this.distance = distance;
        }

        public String getPointFrom() {
            return pointFrom;
        }

        public void setPointFrom(String pointFrom) {
            this.pointFrom = pointFrom;
        }

        public String getPointTo() {
            return pointTo;
        }

        public void setPointTo(String pointTo) {
            this.pointTo = pointTo;
        }

        public LatLng getLatLng() {
            return latLng;
        }

        public void setLatLng(LatLng latLng) {
            this.latLng = latLng;
        }

        public String getBearing() {
            return bearing;
        }

        public void setBearing(String bearing) {
            this.bearing = bearing;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("pointFrom", pointFrom).append("pointTo", pointTo).append("bearing", bearing).append("distance", distance).append("latLng", latLng.toString()).toString();
        }

    }
}


