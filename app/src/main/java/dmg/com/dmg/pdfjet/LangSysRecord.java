package dmg.com.dmg.pdfjet;

class LangSysRecord {
    byte[] langSysTag;  // 4-byte LangSysTag identifier
    int langSysOffset;  // Offset to LangSys table-from beginning of Script table
}
