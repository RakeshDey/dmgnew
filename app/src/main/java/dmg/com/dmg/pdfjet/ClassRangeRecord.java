package dmg.com.dmg.pdfjet;

class ClassRangeRecord {
    int start;          // First GlyphID in the range
    int end;            // Last GlyphID in the range
    int classValue;     // Applied to all glyphs in the range
}
